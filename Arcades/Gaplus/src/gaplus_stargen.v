/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*********************************************
   StarField Generator for "FPGA Gaplus"

				Copyright (c) 2007,2019 MiSTer-X
**********************************************/
module GAPLUS_STARGEN
(
	input       VCLK,
	input       RESET,

	input       VB,

	input [4:0]	C1,
	input [4:0]	C2,
	input [4:0]	C3,

	output reg [7:0] OUT
);

reg        vbtrig;

reg [11:0] sp1,  sp2,  sp3;
reg        sp1d, sp2d, sp3d;

reg [15:0] sLFSR1 = 16'hACE1, LFSR1;
reg [15:0] sLFSR2 = 16'hACE1, LFSR2;
reg [15:0] sLFSR3 = 16'hACE1, LFSR3;

wire [7:0] oSTAR1 = ( LFSR1[15:8] == 8'h80 ) ? LFSR1[7:0] : 0;
wire [7:0] oSTAR2 = ( LFSR2[15:8] == 8'h90 ) ? LFSR2[7:0] : 0;
wire [7:0] oSTAR3 = ( LFSR3[15:8] == 8'hA0 ) ? LFSR3[7:0] : 0;


function [15:0] LFSR;
input [15:0] in;
input			 dir;
	if ( dir ) LFSR = { in[14:0], ((in[15]^in[4])^in[2])^in[1] }; // backward
	else		  LFSR = { ((in[0]^in[2])^in[3])^in[5],  in[15:1] }; // forward
endfunction

always @ ( posedge VCLK or posedge RESET ) begin

	if ( RESET ) begin

		sLFSR1 <= 16'hACE1;
		sLFSR2 <= 16'hACE1;
		sLFSR3 <= 16'hACE1;

		OUT    <= 0;

		vbtrig <= 0;

	end
	else begin

		if ( VB & (~vbtrig) ) begin

			sp1 <= C1[4] ? (12'd384 * C1[2:0]) : C1[2:0]; sp1d <= C1[3];
			sp2 <= C2[4] ? (12'd384 * C2[2:0]) : C2[2:0]; sp2d <= C2[3];
			sp3 <= C3[4] ? (12'd384 * C3[2:0]) : C3[2:0]; sp3d <= C3[3];

			LFSR1 <= sLFSR1;
			LFSR2 <= sLFSR2;
			LFSR3 <= sLFSR3;

			vbtrig  <= 1;

		end
		else begin

			if ( ~VB ) begin
				OUT   <= ( oSTAR1 ? oSTAR1 : ( oSTAR2 ? oSTAR2 : oSTAR3 ) );

				LFSR1 <= LFSR(LFSR1,0);
				LFSR2 <= LFSR(LFSR2,0);
				LFSR3 <= LFSR(LFSR3,0);

				vbtrig <= 0;
			end

			if ( sp1 ) begin sLFSR1 <= LFSR(sLFSR1,~sp1d); sp1 <= sp1-1; end
			if ( sp2 ) begin sLFSR2 <= LFSR(sLFSR2,~sp2d); sp2 <= sp2-1; end
			if ( sp3 ) begin sLFSR3 <= LFSR(sLFSR3,~sp3d); sp3 <= sp3-1; end

		end

	end

end

endmodule

