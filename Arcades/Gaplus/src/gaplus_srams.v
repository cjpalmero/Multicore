/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//**********************************************
   Share memory module for "FPGA Gaplus"

				Copyright (c) 2007,2019 MiSTer-X
***********************************************/
module GAPLUS_SHAREMEM
(
	input         clk,
	input			  vclk,

	input         sel,

	input  [15:0] ad0,
	output  [7:0] rd0,
	input   [7:0] wd0,
	input         we0,

	input  [15:0] ad1,
	output  [7:0] rd1,
	input   [7:0] wd1,
	input         we1,

	input   [9:0] vram_a,
	output [15:0] vram_d,

	input   [6:0] spra_a,
	output [23:0] spra_d
);

wire  [6:0] dum;
wire [15:0] ad;
wire  [7:0] wd;
wire        we;

busdriver arb( 1'b1,
	sel,
	{ 7'h0, ad0[15:0], wd0[7:0], we0 },
	{ 7'h0, ad1[15:0], wd1[7:0], we1 },
	{  dum,  ad[15:0],  wd[7:0], we  }
);

wire	[7:0] o3I, o3J, o3M, o3K, o3L;

wire	e3I = ( ad[15:10] == 6'b000000 );
wire	e3J = ( ad[15:10] == 6'b000001 );
wire	e3M = ( ad[15:11] == 5'b00001  );
wire	e3K = ( ad[15:11] == 5'b00010  );
wire	e3L = ( ad[15:11] == 5'b00011  );

wire [7:0] rd = e3I ? o3I :
					 e3J ? o3J :
					 e3M ? o3M :
					 e3K ? o3K :
					 e3L ? o3L :
					 8'hFF;

DPRAM_1024V sram3I( clk, ad[9:0],  wd, o3I, we & e3I, vclk, vram_a, vram_d[7:0]  );
DPRAM_1024V sram3J( clk, ad[9:0],  wd, o3J, we & e3J, vclk, vram_a, vram_d[15:8] );

DPRAM_2048V sram3M( clk, ad[10:0], wd, o3M, we & e3M, vclk, { 4'b1111, spra_a }, spra_d[7:0]   );
DPRAM_2048V sram3K( clk, ad[10:0], wd, o3K, we & e3K, vclk, { 4'b1111, spra_a }, spra_d[15:8]  );
DPRAM_2048V sram3L( clk, ad[10:0], wd, o3L, we & e3L, vclk, { 4'b1111, spra_a }, spra_d[23:16] );

assign rd0 = rd;
assign rd1 = rd;

endmodule

