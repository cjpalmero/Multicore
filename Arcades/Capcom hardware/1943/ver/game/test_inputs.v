/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns/1ps

module test_inputs(
    input   loop_rst,
    input   LVBL,
    output  reg [6:0] game_joystick1,
    output  reg       button_1p,
    output  reg       coin_left
);

localparam FIRE = 4;
localparam DOWN = 2;

integer framecnt=0;

always @(negedge loop_rst)
    $display("INFO: loop_rst over.");

always @(negedge LVBL)
    if( loop_rst ) begin
        game_joystick1 <= ~7'd0;
        button_1p      <= 1'b1;
        coin_left      <= 1'b0;
    end else begin
        framecnt <= framecnt + 1;
        case( framecnt>>3 )
            0: coin_left <= 1'b0;   // enable all test modes
            4: begin
                coin_left <= 1'b1;
                game_joystick1[DOWN] <= 1'b0;
            end
            8: begin
                game_joystick1[DOWN] <= 1'b1;
                game_joystick1[FIRE] <= 1'b0;
            end
            9: game_joystick1[FIRE] <= 1'b1;
        endcase // framecnt
    end

endmodule // test_inputs