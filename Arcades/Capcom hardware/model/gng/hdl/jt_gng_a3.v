/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns/1ps

/*

	Schematic sheet: 85606-A- -1/8 CPU

*/

module jt_gng_a3(
	input			INCS_b,
	input	[2:0]	AB,
	inout	[7:0]	DB,
	input	[1:0]	UP,
	input	[1:0]	DOWN,
	input	[1:0]	LEFT,
	input	[1:0]	RIGHT,
	input	[1:0]	SHOT2,
	input	[1:0]	SHOT1,
	inout	[7:0]	DIPSW_B,
	inout	[7:0]	DIPSW_A,
	input	[1:0]	COIN,
	input	[1:0]	START,
	input	[1:0]	RESERVED
);

wire [7:0] IN;
jt74138 u_3A (.e1_b(INCS_b), .e2_b(INCS_b), .e3(1'b1), .a(AB[2:0]), .y_b(IN));

jt74367 u_4A (
	.A( { SHOT2[0], SHOT1[0], UP[0], DOWN[0], LEFT[0], RIGHT[0] }  ),
	.Y(DB[5:0]), .en4_b(IN[1]), .en6_b(IN[1])
);

jt74367 u_5A (
	.A( { SHOT2[1], SHOT1[1], UP[1], DOWN[1], LEFT[1], RIGHT[1] }  ),
	.Y		(DB[5:0]	),
	.en4_b	(IN[2]		),
	.en6_b	(IN[2]		)
);

jt74245 u_2B (
	.a( DIPSW_B  ),
	.b(DB[7:0]), .dir(IN[4]), .en_b(IN[4])
);


jt74245 u_1B (
	.a( DIPSW_A  ),
	.b(DB[7:0]), .dir(1'b0), .en_b(IN[3])
);

jt74367 u_8A (
	.A( {COIN[1:0], RESERVED, START}  ),
	.Y( {DB[7:4],DB[1:0]} ), .en4_b(IN[0]), .en6_b(IN[0])
);



endmodule // jt_gng_a3