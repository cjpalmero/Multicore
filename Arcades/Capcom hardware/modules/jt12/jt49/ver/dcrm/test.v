/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns / 1ps

module test;

reg clk, rst;

initial begin
    clk = 1'b0;
    forever clk = #12500 ~clk;
end

integer simcnt;

initial begin
    rst = 1'b1;
    #30000 rst=1'b0;
    $display("Reset over: simulation starts");
    for( simcnt=0; simcnt<1000; simcnt=simcnt+1)
        #1000_000;
    $finish;
end

wire signed [7:0] dout;

reg [15:0] cnt;
wire [7:0] din;

always @(posedge clk)
    if( rst ) begin
        cnt <= 0;
    end else begin
        cnt <= cnt + 1;
    end

assign din = cnt[10:5]  + cnt[3:0];
// assign din = cnt[7:0];

jt49_dcrm2 UUT(
    .clk  ( clk   ),
    .cen  ( 1'b1  ),
    .rst  ( rst   ),
    .din  ( din   ),
    .dout ( dout  )
);

`ifndef NCVERILOG
    initial begin
        $display("DUMP enabled");
        $dumpfile("test.lxt");
        $dumpvars;
        $dumpon;
    end
`else
    initial begin
        $display("NC Verilog: will dump all signals");
        $shm_open("test.shm");
        $shm_probe(UUT,"AS");
    end
`endif

endmodule