/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//  Arcade: Asteroids
//
//  Port to MiSTer
//  Copyright (C) 2019 
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//---------------------------------------------------------------------------------//
//
//  Top for Multicore
//  2019 - Victor Trucco
//
//

`default_nettype none

module Asteroids_mc (
	// Clocks
	input wire	clock_50_i,

	// Buttons
	input wire [4:1]	btn_n_i,

	// SRAMs (AS7C34096)
	output wire	[18:0]sram_addr_o  = 18'b0000000000000000000,
	inout wire	[7:0]sram_data_io	= 8'bzzzzzzzz,
	output wire	sram_we_n_o		= 1'b1,
	output wire	sram_oe_n_o		= 1'b1,

	// PS2
	inout wire	ps2_clk_io			= 1'bz,
	inout wire	ps2_data_io			= 1'bz,
	inout wire	ps2_mouse_clk_io  = 1'bz,
	inout wire	ps2_mouse_data_io = 1'bz,

	// SD Card
	output wire	sd_cs_n_o			= 1'b1,
	output wire	sd_sclk_o			= 1'b0,
	output wire	sd_mosi_o			= 1'b0,
	input wire	sd_miso_i,

	// Joysticks
	input wire	joy1_up_i,
	input wire	joy1_down_i,
	input wire	joy1_left_i,
	input wire	joy1_right_i,
	input wire	joy1_p6_i,
	input wire	joy1_p9_i,
	input wire	joy2_up_i,
	input wire	joy2_down_i,
	input wire	joy2_left_i,
	input wire	joy2_right_i,
	input wire	joy2_p6_i,
	input wire	joy2_p9_i,
	output wire	joyX_p7_o			= 1'b1,

	// Audio
	output wire	dac_l_o				= 1'b0,
	output wire	dac_r_o				= 1'b0,
	input wire	ear_i,
	output wire	mic_o					= 1'b0,

		// VGA
	output wire	[2:0]vga_r_o,
	output wire	[2:0]vga_g_o,
	output wire	[2:0]vga_b_o,
	output wire	vga_hsync_n_o,
	output wire	vga_vsync_n_o 
);



wire clk_6, clk_25,clk_50;
wire pll_locked;


pll pll(
	.inclk0(clock_50_i),
	.c0(clk_6),
	.c1(clk_25),
	.c2(clk_50),
	.locked(pll_locked)
	);



wire [31:0] status;
wire  [1:0] buttons;

wire btn_right = joyBCPPFRLDU[3];
wire btn_left = joyBCPPFRLDU[2];
wire btn_one_player = ~btn_n_i[1] | joyBCPPFRLDU[5];
wire btn_two_players = ~btn_n_i[2] | joyBCPPFRLDU[6];
wire btn_fire = joyBCPPFRLDU[4];
wire btn_coin = btn_n_i[3] | joyBCPPFRLDU[7];
wire btn_thrust = joyBCPPFRLDU[0];
wire btn_shield = joyBCPPFRLDU[8];
wire btn_start_1=0;

wire [7:0] BUTTONS = {~btn_right & joy1_right_i,~btn_left & joy1_left_i,~(btn_one_player|btn_start_1),~btn_two_players,~btn_fire & joy1_p6_i,~btn_coin,~btn_thrust & joy1_up_i,~btn_shield & joy1_down_i};
wire hblank, vblank;

wire ce_vid = 1; 
wire hs, vs;
wire [2:0] r,g;
wire [2:0] b;


assign vga_r_o    = {r,r,r[2]};
assign vga_g_o    = {g,g,g[2]};
assign vga_b_o    = {b,b,b[2]};
assign vga_hsync_n_o   = ~hs;
assign vga_vsync_n_o   = ~vs;

wire reset = ~btn_n_i[4]; 

wire [7:0] audio;
assign dac_l_o = {audio, audio};
assign dac_r_o = {audio, audio};

wire [1:0] lang = status[4:3];
wire  ships = ~status[6];

wire vgade;

ASTEROIDS_TOP ASTEROIDS_TOP
(

	.BUTTON(BUTTONS),
	.SELF_TEST_SWITCH_L(~status[7]), 
	.LANG(lang),
	.SHIPS(ships),
	.AUDIO_OUT(audio),

	.VIDEO_R_OUT(r),
	.VIDEO_G_OUT(g),
	.VIDEO_B_OUT(b),
	.HSYNC_OUT(hs),
	.VSYNC_OUT(vs),
	.VGA_DE(vgade),
	.VID_HBLANK(hblank),
	.VID_VBLANK(vblank),

	.RESET_L (~reset),	
	.clk_6(clk_6),
	.clk_25(clk_25),
	.clk_50(clk_50),

	.sram_addr  (sram_addr_o),
	.sram_data  (sram_data_io),  
	.sram_we_n  (sram_we_n_o)
);

assign sram_oe_n_o = 1'b0;


wire kbd_intr;
wire [8:0] joyBCPPFRLDU;
wire [7:0] kbd_scancode;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_25 ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

//translate scancode to joystick
kbd_joystick k_joystick
(
  .clk         	(  clk_25 ),
  .kbdint      	(  kbd_intr ),
  .kbdscancode 	(  kbd_scancode ), 
  .joyBCPPFRLDU   ( joyBCPPFRLDU )
);



endmodule
