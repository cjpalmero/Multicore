--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

library work;
use work.pace_pkg.all;
use work.video_controller_pkg.all;
use work.platform_pkg.all;

entity target_top is
  port(
    clk_sys      	: in std_logic;
    clk_vid_en    : in std_logic;
    reset_in      : in std_logic;
    snd_l         : out std_logic_vector(9 downto 0);
    snd_r         : out std_logic_vector(9 downto 0);
    vid_hs        : out std_logic;
    vid_vs        : out std_logic;
    vid_hb        : out std_logic;
    vid_vb        : out std_logic;
    vid_r         : out std_logic_vector(3 downto 0);
    vid_g         : out std_logic_vector(3 downto 0);
    vid_b         : out std_logic_vector(3 downto 0);	
    inputs_p1     : in std_logic_vector(7 downto 0);
    inputs_p2     : in std_logic_vector(7 downto 0);
    inputs_sys    : in std_logic_vector(7 downto 0);
    inputs_dip1   : in std_logic_vector(7 downto 0);
    inputs_dip2   : in std_logic_vector(7 downto 0);	
    cpu_rom_addr  : out std_logic_vector(15 downto 0);
    cpu_rom_do    : in std_logic_vector(7 downto 0);
    tile_rom_addr : out std_logic_vector(12 downto 0);
    tile_rom_do   : in std_logic_vector(15 downto 0);
    snd_rom_addr : out std_logic_vector(12 downto 0);
    snd_rom_do   : in std_logic_vector(7 downto 0)
  );    

end target_top;

architecture SYN of target_top is

  signal clkrst_i       : from_CLKRST_t;
  signal video_i        : from_VIDEO_t;
  signal video_o        : to_VIDEO_t;
  signal audio_i        : from_AUDIO_t;
  signal audio_o        : to_AUDIO_t;
  signal platform_i     : from_PLATFORM_IO_t;
  signal platform_o     : to_PLATFORM_IO_t;


begin

clkrst_i.clk(0) <=clk_sys;
clkrst_i.clk(1) <= clk_sys;
clkrst_i.arst <= reset_in;
clkrst_i.arst_n <= not clkrst_i.arst;

video_i.clk <= clk_sys;
video_i.clk_ena <= clk_vid_en;
video_i.reset <= reset_in;

  GEN_RESETS : for i in 0 to 3 generate

    process (clkrst_i)
      variable rst_r : std_logic_vector(2 downto 0) := (others => '0');
    begin
      if clkrst_i.arst = '1' then
        rst_r := (others => '1');
      elsif rising_edge(clkrst_i.clk(i)) then
        rst_r := rst_r(rst_r'left-1 downto 0) & '0';
      end if;
      clkrst_i.rst(i) <= rst_r(rst_r'left);
    end process;

  end generate GEN_RESETS;
   
vid_r <= video_o.rgb.r(9 downto 6);
vid_g <= video_o.rgb.g(9 downto 6);
vid_b <= video_o.rgb.b(9 downto 6);
vid_hs <= video_o.hsync;
vid_vs <= video_o.vsync;
vid_hb <= video_o.hblank;
vid_vb <= video_o.vblank;
snd_l <= audio_o.ldata(9 downto 0);
snd_r <= audio_o.rdata(9 downto 0);

pace_inst : entity work.pace                                            
  port map(
    clkrst_i				=> clkrst_i,
    inputs_p1       	=> inputs_p1, 
    inputs_p2       	=> inputs_p2, 
    inputs_sys       	=> inputs_sys,
    inputs_dip1       => inputs_dip1, 
    inputs_dip2       => inputs_dip2, 
    video_i           => video_i,
    video_o           => video_o,
    audio_i           => audio_i,
    audio_o           => audio_o,
    platform_i        => platform_i,
    platform_o        => platform_o,	
    cpu_rom_addr      => cpu_rom_addr,
    cpu_rom_do	      => cpu_rom_do,
    tile_rom_addr     => tile_rom_addr,
    tile_rom_do	      => tile_rom_do,
    snd_rom_addr      => snd_rom_addr,
    snd_rom_do        => snd_rom_do
    );

end SYN;
