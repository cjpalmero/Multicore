quartus_sh -t rtl/build_id.tcl DEFENDER 
quartus_sh --flow compile DefenderHardware.qpf
copy output_files\DefenderHardware.rbf output_files\Defender.mc2

quartus_sh -t rtl/build_id.tcl COLONY7 
quartus_sh --flow compile DefenderHardware.qpf
copy output_files\DefenderHardware.rbf output_files\Colony7.mc2

quartus_sh -t rtl/build_id.tcl MAYDAY 
quartus_sh --flow compile DefenderHardware.qpf
copy output_files\DefenderHardware.rbf output_files\Mayday.mc2

quartus_sh -t rtl/build_id.tcl JIN 
quartus_sh --flow compile DefenderHardware.qpf
copy output_files\DefenderHardware.rbf output_files\Jin.mc2

pause