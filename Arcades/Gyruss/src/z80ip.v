/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// Copyright (c) 2020 MiSTer-X

module Z80IP
(
	input				reset,
	input				clk,
	output [15:0]	adr,
	input   [7:0]	din,
	output  [7:0]	dout,
	output			m1,
	output			mr,
	output			mw,
	output			ir,
	output			iw,

	input				intreq,
	output reg		intrst,

	input				nmireq,
	output reg		nmirst
);

wire i_mreq, i_iorq, i_rd, i_wr, i_rfsh, i_m1; 

T80s cpu (
	.CLK_n(clk),
	.RESET_n(~reset),
	.INT_n(~intreq),
	.NMI_n(~nmireq),
	.MREQ_n(i_mreq),
	.IORQ_n(i_iorq),
	.RFSH_n(i_rfsh),
	.RD_n(i_rd),
	.WR_n(i_wr),
	.A(adr),
	.DI(din),
	.DO(dout),
	.WAIT_n(1'b1),
	.BUSRQ_n(1'b1),
	.BUSAK_n(),
	.HALT_n(),
	.M1_n(i_m1)
);

assign m1 = (~i_m1);
wire mreq = (~i_mreq) & i_rfsh;
wire iorq = (~i_iorq) & i_m1;
wire rdr  = (~i_rd);
wire wrr  = (~i_wr);

assign mr = mreq & rdr;
assign mw = mreq & wrr;

assign ir = iorq & rdr;
assign iw = iorq & wrr;

always @(posedge clk) begin
	if (reset) begin
		intrst <= 0;
		nmirst <= 0;
	end
	else begin
		intrst <= (adr==16'h38) & m1 & mr;
		nmirst <= (adr==16'h66) & m1 & mr;
	end
end

endmodule

