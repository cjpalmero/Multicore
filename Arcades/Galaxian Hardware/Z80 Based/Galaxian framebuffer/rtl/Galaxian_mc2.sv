/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//  Arcade: Galaxian
//
//  Version With Framebuffer and HDMI
//  Copyright (C) 2020 Victor Trucco
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================

`default_nettype none

module Galaxian_mc2
(
   // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output wire [18:0]sram_addr_o  = 18'b0000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
    input wire  joy1_up_i,
    input wire  joy1_down_i,
    input wire  joy1_left_i,
    input wire  joy1_right_i,
    input wire  joy1_p6_i,
    input wire  joy1_p9_i,
    input wire  joy2_up_i,
    input wire  joy2_down_i,
    input wire  joy2_left_i,
    input wire  joy2_right_i,
    input wire  joy2_p6_i,
    input wire  joy2_p9_i,
    output wire joyX_p7_o           = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        // HDMI
    output wire [7:0]tmds_o         = 8'b00000000,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
        
    inout wire  stm_b8_io, 
    inout wire  stm_b9_io,

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2
);

`include "rtl\build_id.v" 

`define NAME "GALAXIAN"
reg [6:0] core_mod;
reg rotate_dir;

always @(*) 
begin

  //  core_mod = 7'd2;
    
    if (core_mod == 7'd1  || // "MOONCR"
        core_mod == 7'd6  || // "DEVILFSH"
        core_mod == 7'd9  || // "OMEGA"
        core_mod == 7'd10 || // "ORBITRON"
        core_mod == 7'd13)   // "VICTORY"
    begin
        rotate_dir = 1'b0;
    end else begin
        rotate_dir = 1'b1;
    end
end

localparam CONF_STR = {
       "P,galaxian.dat;",
    //   "P,mooncrgx.dat;",
  //  "P,Azurian Attack.dat;",
 //   "P,CORE_NAME.dat;",
    "O23,Scanlines,Off,25%,50%,75%;",
    "O78,Rotate Screen,Off,90,180,270;", 
    "T0,Reset;",
    "V,v2.00.",`BUILD_DATE
};

assign stm_rst_o        = 1'bz;

wire       rotate    = status[1];
wire [1:0] scanlines_ctrl = status[3:2];
wire       blend     = status[4];

assign AUDIO_R = AUDIO_L;

wire clk_24, clk_12, clk_6, clk_pixel, clk_dvi;

wire [1:0] pll_locked_in;
wire pll_locked;
wire pll_areset;
wire pll_scanclk;
wire pll_scandata;
wire pll_scanclkena ;
wire pll_configupdate;
wire pll_scandataout;
wire pll_scandone;
wire [7:0]pll_rom_address;
wire pll_write_rom_ena;
wire pll_write_from_rom;
wire pll_reconfig_s;
wire pll_reconfig_busy;
wire pll_reconfig_reset;
reg [1:0]pll_reconfig_state = 2'b00;
integer pll_reconfig_timeout;
wire pll_rom_q;

pll pll
(
    .inclk0         ( clock_50_i ),
    .c0(clk_24),
    .c1(clk_12),
    .c2(clk_6),
    .c3(clk_pixel),
    .c4(clk_dvi),
    .locked         ( pll_locked ),

    .areset         ( pll_areset ),
    .scanclk        ( pll_scanclk ),
    .scandata       ( pll_scandata ),
    .scanclkena     ( pll_scanclkena ),
    .configupdate   ( pll_configupdate ),
    .scandataout    ( pll_scandataout ),
    .scandone       ( pll_scandone )

);

pll_reconfig pll_reconfig
(
    .busy               ( pll_reconfig_busy ),
    .clock              ( clock_50_i ),
    .counter_param      ( 3'b000 ),
    .counter_type       ( 4'b0000 ),
    .data_in            ( 9'b000000000 ),
    .pll_areset         ( pll_areset ),
    .pll_areset_in      ( 0 ),
    .pll_configupdate   ( pll_configupdate),
    .pll_scanclk        ( pll_scanclk ),
    .pll_scanclkena     ( pll_scanclkena ),
    .pll_scandata       ( pll_scandata ),
    .pll_scandataout    ( pll_scandataout ),
    .pll_scandone       ( pll_scandone ),
    .read_param         ( 0 ),
    .reconfig           ( pll_reconfig_s ),
    .reset              ( pll_reconfig_reset ),
    .reset_rom_address  ( 0 ),
    .rom_address_out    ( pll_rom_address ),
    .rom_data_in        ( pll_rom_q ),
    .write_from_rom     ( pll_write_from_rom ),
    .write_param         ( 0 ),
    .write_rom_ena      ( pll_write_rom_ena )
);

wire q_reconfig_27, q_reconfig_40;
    
reconfig_27  reconfig_27  ( .address ( pll_rom_address ), .clock ( clock_50_i ), .rden ( pll_write_rom_ena ), .q ( q_reconfig_27 ));
reconfig_40  reconfig_40  ( .address ( pll_rom_address ), .clock ( clock_50_i ), .rden ( pll_write_rom_ena ), .q ( q_reconfig_40 ));

wire img_rotate = status[7];
reg  old_rotate = 1'b1;
reg  recfg = 1'b0;

always @(*) 
begin
    case(img_rotate)
        1'b0: begin pll_rom_q <= q_reconfig_27; end
        1'b1: begin pll_rom_q <= q_reconfig_40; end
    endcase
end   

always @(posedge clock_50_i) 
begin

    pll_write_from_rom <= 0;
    pll_reconfig_s <= 0;
    pll_reconfig_reset <= 0;

    case(pll_reconfig_state)

        0: begin
                if (recfg)
                begin
                    pll_write_from_rom <= 1;
                    pll_reconfig_state <= 1;
                end
            end

        1: begin
                pll_reconfig_state <= 2;
            end

        2: begin
        
                if (pll_reconfig_busy == 0)
                begin   
                    pll_reconfig_s <= 1;
                    pll_reconfig_state <= 3;
                    pll_reconfig_timeout <= 1000;
                end;
                
            end

        3: begin
        
                pll_reconfig_timeout <= pll_reconfig_timeout - 1;
                
                if (pll_reconfig_timeout == 1) 
                begin
                    pll_reconfig_reset <= 1; // sometimes pll reconfig stuck in busy state
                    pll_reconfig_state <= 0;
                    recfg <= 0;
                end
                
                if (pll_reconfig_s == 0 && pll_reconfig_busy == 0)
                begin
                    pll_reconfig_state <= 0;
                    recfg <= 0;
                end
                
            end
    endcase

    if( img_rotate != old_rotate ) 
    begin
        old_rotate <= img_rotate;
        recfg <= 1;
    end

end  

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;

/*
user_io #(
    .STRLEN(($size(CONF_STR)>>3)))
user_io(
    .clk_sys        (clk_12         ),
    .conf_str       (CONF_STR       ),
    .SPI_CLK        (SPI_SCK        ),
    .SPI_SS_IO      (CONF_DATA0     ),
    .SPI_MISO       (SPI_DO         ),
    .SPI_MOSI       (SPI_DI         ),
    .buttons        (buttons        ),
    .switches       (switches       ),
    .scandoubler_disable (scandoublerD ),
    .ypbpr          (ypbpr          ),
    .no_csync       (no_csync       ),
    .core_mod       (core_mod       ),
    .key_strobe     (key_strobe     ),
    .key_pressed    (key_pressed    ),
    .key_code       (key_code       ),
    .joystick_0     (joystick_0     ),
    .joystick_1     (joystick_1     ),
    .status         (status         )
    );
*/
/* ROM Structure
0000 - 3FFF PGM ROM 16k
4000 - 4FFF ROM 1K   4k
5000 - 5FFF ROM 1H   4k
6000 - 601F ROM 6L  32b
*/

wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

data_io #(.STRLEN (($size(CONF_STR)>>3))) data_io(
    .clk_sys       ( clk_12       ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( keys_s & pump_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    .core_mod      ( core_mod     ),

    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

wire  [7:0] audio_a, audio_b, audio_c;
wire [10:0] audio = {1'b0, audio_b, 2'b0} + {3'b0, audio_a} + {2'b00, audio_c, 1'b0};
wire        hs, vs;
wire        hb, vb;
wire        blankn = ~(hb | vb);
wire  [2:0] r,g,b;
wire  [4:0] idx_color_s;

galaxian galaxian
(
    .W_CLK_24M(clk_24),
    .W_CLK_12M(clk_12),
    .W_CLK_6M(clk_6),
    .I_RESET(status[0] | ~btn_n_i[4] | ioctl_downl),
    .I_HWSEL(core_mod),

    .I_DL_ADDR(ioctl_addr[15:0]),
    .I_DL_WR(ioctl_wr),
    .I_DL_DATA(ioctl_dout),

    .I_TABLE(status[5]),
    .I_TEST(status[6]),

    .I_SERVICE(status[9]), // <-----------------------------
    

    .I_SW1_67(status[15:14]),
    .I_DIP(status[23:16]),
    .P1_CSJUDLR({btn_coin,btn_one_player,m_fireA,m_up,m_down,m_left,m_right}),
    .P2_CSJUDLR({1'b0,btn_two_players,m_fire2A,m_up2,m_down2,m_left2,m_right2}),

    .video_x_o          ( video_x_s ),
    .video_y_o          ( video_y_s ),

    .W_MISSILE (W_MISSILE),
    .W_SHELL   (W_SHELL),
    .W_STARS   (W_STARS),

    .CLUT_ADDR_O (idx_color_s),
    .W_R(r),
    .W_G(g),
    .W_B(b),
    .W_H_SYNC(hs),
    .W_V_SYNC(vs),
    .HBLANK(hb),
    .VBLANK(vb),

    .W_SDAT_A(audio_a),
    .W_SDAT_B(audio_b),
    .W_SDAT_C(audio_c),

    // external roms
    .W_CPU_ROM_ADDR  ( rom_addr_s ),
    .W_CPU_ROM_DO    ( rom_dout_s ),
    .ROM_CS_O        ( rom_cs_s   ),
    .GFX_ADDR_O      ( gfx_addr_s ),
    .GFX_DATA_I      ( gfx_data_s ),
    .explosion_addr_o ( explosion_addr_s ),
    .explosion_data_i ( explosion_data_s )

);

wire W_MISSILE, W_SHELL;
wire [4:0] W_STARS;

wire [11:0] gfx_addr_s;
wire [12:0] explosion_addr_s;
wire [13:0] rom_addr_s;
wire [ 7:0] rom_dout_s, gfx_data_s,explosion_data_s;
wire rom_cs_s;


wire [2:0] comb_r = r | color_s[2:0]; 
wire [2:0] comb_g = g | color_s[5:3]; 
wire [2:0] comb_b = b | {color_s[7:6], 1'b0}; 

wire [5:0] vga_r_s; 
wire [5:0] vga_g_s; 
wire [5:0] vga_b_s; 
wire vga_hs_s, vga_vs_s;


mist_video #(.COLOR_DEPTH(3),.SD_HCNT_WIDTH(10)) mist_video
(
    .clk_sys    ( clk_pixel  ),

    .SPI_SCK    ( SPI_SCK ),
    .SPI_SS3    ( SPI_SS2 ),
    .SPI_DI     ( SPI_DI  ),

    .R          ( vga_color_s[2:0] ),
    .G          ( vga_color_s[5:3] ),
    .B          ( {vga_color_s[7:6], 1'b0} ),
    .HSync      ( vga_hs_s ),
    .VSync      ( vga_vs_s ),

    .VGA_R      ( vga_r_s  ),
    .VGA_G      ( vga_g_s  ),
    .VGA_B      ( vga_b_s  ),
    .VGA_HS     (  ),
    .VGA_VS     (  ),
    
    .ce_divider ( 1'b1 ),
    .rotate     ( {(rotate_dir)?~status[8]:status[8], ~status[7]} ), //{rotate_dir,rotate} ),
    .scandoubler_disable ( 1'b1 ),
    .scanlines  ( 1'b0 ),
    .blend      ( blend ),
    .no_csync   ( no_csync ),
    .osd_enable ( osd_enable )
);

    wire [5:0] vga_r_out_s; 
    wire [5:0] vga_g_out_s; 
    wire [5:0] vga_b_out_s; 
    
    reg scanline = 0;
    
    //scanlines
    always @(posedge clk_pixel) 
    begin

        // if no scanlines_ctrl or not a scanline
        if(!scanline || !scanlines_ctrl) 
        begin
            vga_r_out_s <= vga_r_s;
            vga_g_out_s <= vga_g_s;
            vga_b_out_s <= vga_b_s;
        end else 
        begin
            case(scanlines_ctrl)
                2'b01: begin // reduce 25% = 1/2 + 1/4
                    vga_r_out_s <= {1'b0, vga_r_s[5:1]} + {2'b00, vga_r_s[5:2] };
                    vga_g_out_s <= {1'b0, vga_g_s[5:1]} + {2'b00, vga_g_s[5:2] };
                    vga_b_out_s <= {1'b0, vga_b_s[5:1]} + {2'b00, vga_b_s[5:2] };
                end

                2'b10: begin // reduce 50% = 1/2
                    vga_r_out_s <= {1'b0, vga_r_s[5:1]};
                    vga_g_out_s <= {1'b0, vga_g_s[5:1]};
                    vga_b_out_s <= {1'b0, vga_b_s[5:1]};
                end

                2'b11: begin // reduce 75% = 1/4
                    vga_r_out_s <= {2'b00, vga_r_s[5:2]};
                    vga_g_out_s <= {2'b00, vga_g_s[5:2]};
                    vga_b_out_s <= {2'b00, vga_b_s[5:2]};
                end
            endcase
        end
    end

    wire scandblctrl = btn_n_i[2];
            
    assign VGA_R  = ( scandblctrl ) ? vga_r_out_s[5:1] : {comb_r, 2'b00};
    assign VGA_G  = ( scandblctrl ) ? vga_g_out_s[5:1] : {comb_g, 2'b00};
    assign VGA_B  = ( scandblctrl ) ? vga_b_out_s[5:1] : {comb_b, 2'b00};
    assign VGA_HS = ( scandblctrl ) ? vga_hs_s         : hs;
    assign VGA_VS = ( scandblctrl ) ? vga_vs_s         : vs;


dac #(11)
dac(
    .clk_i(clk_12),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(AUDIO_L)
    );

//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player =   ~btn_n_i[1] | m_one_player;
wire btn_two_players =  ~btn_n_i[2] | m_two_players;
wire btn_coin  =        ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_12 ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 )) k_joystick
(
    .clk         ( clk_12 ),
    .kbdint      ( kbd_intr ),
    .kbdscancode ( kbd_scancode ), 

    .joystick_0  ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1  ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),

    //-- joystick_0 and joystick_1 should be swapped
    .joyswap     ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer   ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls    ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right
    .player1     ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2     ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    //-- keys to the OSD
    .osd_o       ( keys_s ),
    .osd_enable  ( osd_enable ),

    //-- sega joystick
    .sega_clk    ( hs ),
    .sega_strobe ( joyX_p7_o ) 
);

//--------- ROM DATA PUMP ----------------------------------------------------
reg [15:0] power_on_s   = 16'b1111111111111111;
reg [7:0] pump_s = 8'b11111111;

reg first_reset = 1'b1;

//--start the microcontroller OSD menu after the power on
always @(posedge clk_12) 
begin

        if (first_reset == 1)
        begin
            power_on_s = 16'b1111111111111111;
            first_reset = 1'b0;
        end
        else if (power_on_s != 0)
        begin
            power_on_s = power_on_s - 1;
            pump_s = 8'b00111111;
        end 
            
        
        if (ioctl_downl == 1 && pump_s == 8'b00111111)
            pump_s = 8'b11111111;
    
end 

//--------- CLUT ----------------------------------------------------
dpram #(5,8)  clut 
(
    .clock_a   ( clk_12 ),
    .wren_a    ( ioctl_wr & (ioctl_addr[15:5] == 11'b10000000000)),// 8000-801f    era 6000-601F
    .address_a ( ioctl_addr[4:0] ),
    .data_a    ( ioctl_dout ),

    .clock_b   ( clk_12 ),
    .address_b ( idx_color_s ),
    .q_b       ( color_s )
);

wire [7:0] color_s;


reg [4:0] missile_color_s =5'b11111;

always @(*)
begin

    case (core_mod)
        7'd0:    begin missile_color_s = 5'b00011; end // Galaxian
        7'd1:    begin missile_color_s = 5'b11111; end // Moon Cresta
        default: begin missile_color_s = 5'b11111; end // last palette color
    endcase

    // HW_AZURIAN  : integer := 2;
    // HW_BLKHOLE  : integer := 3;
    // HW_CATACOMB : integer := 4;
    // HW_CHEWINGG : integer := 5;
    // HW_DEVILFSH : integer := 6;
    // HW_KINGBAL  : integer := 7;
    // HW_MRDONIGH : integer := 8;
    // HW_OMEGA    : integer := 9;
    // HW_ORBITRON : integer := 10;
    // HW_PISCES   : integer := 11;
    // HW_UNIWARS  : integer := 12;
    // HW_VICTORY  : integer := 13;
    // HW_WAROFBUG : integer := 14;
    // HW_ZIGZAG   : integer := 15; -- doesn't work yet
    // HW_TRIPLEDR : integer := 16;
end


wire [4:0] idx1 = (W_MISSILE) ? missile_color_s : (W_STARS == 5'b00000) ? idx_color_s : W_STARS;

//--------- VGA/HDMI OUTPUT ----------------------------------------------------
    wire [8:0] video_x_s;
    wire [7:0] video_y_s;

    wire [4:0] vga_col_s;
    wire vga_blank_s, vga_out_hs_s, vga_out_vs_s;
    
    
    vga vga
    (
        .I_CLK      ( clk_12 ),
        .I_CLK_VGA  ( clk_pixel ),
        .I_COLOR    ( idx1 ),//idx_color_s ),
        .I_HCNT     ( video_x_s ),
        .I_VCNT     ( video_y_s ),
        
        .I_ROTATE   ( status[8:7] ),
        
        .O_HSYNC    ( vga_hs_s ),
        .O_VSYNC    ( vga_vs_s ),
        .O_COLOR    ( vga_col_s ),
        .O_BLANK    ( vga_blank_s ),

        .O_ODD_LINE ( scanline )
    );
    

//--------- CLUT VGA ----------------------------------------------------



dpram #(5,8)  clut_vga 
(
    .clock_a   ( clk_12 ),
    .wren_a    ( ioctl_wr & (ioctl_addr[15:5] == 11'b10000000000)),// 8000-801f    era 6000-601F
    .address_a ( ioctl_addr[4:0] ),
    .data_a    ( ioctl_dout ),

    .clock_b   ( clk_pixel ),
    .address_b ( vga_col_s ),
    .q_b       ( vga_color_s )
);

wire [7:0] vga_color_s;

/*
wire scandblctrl = ~btn_n_i[2];
           
assign VGA_R  = ( scandblctrl ) ? vga_r_s[5:1] : {vga_color_s[2:0], 2'b00};
assign VGA_G  = ( scandblctrl ) ? vga_g_s[5:1] : {vga_color_s[5:3], 2'b00};
assign VGA_B  = ( scandblctrl ) ? vga_b_s[5:1] : {vga_color_s[7:6], 3'b000};
assign VGA_HS = ( scandblctrl ) ? vga_hs_s     : vga_out_hs_s    ;
assign VGA_VS = ( scandblctrl ) ? vga_vs_s     : vga_out_vs_s    ;
*/

wire    [15:0] sound_hdmi_s;
    
    wire [9:0] tdms_r_s;
    wire [9:0] tdms_g_s;
    wire [9:0] tdms_b_s;
    wire [3:0] hdmi_p_s;
    wire [3:0] hdmi_n_s;
    
 //-- HDMI
    hdmi 
    # (
        .FREQ ( 40000000 ),   //-- pixel clock frequency 
        .FS   ( 48000 ),      //-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
        .CTS  ( 40000 ),      //-- CTS = Freq(pixclk) * N / (128 * Fs)
        .N    ( 6144 )        //-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
    ) inst_dvid
    (
        .I_CLK_PIXEL    ( clk_pixel ),
        .I_R            ( {vga_r_out_s, vga_r_out_s[5:4]} ),
        .I_G            ( {vga_g_out_s, vga_g_out_s[5:4]} ),
        .I_B            ( {vga_b_out_s, vga_b_out_s[5:4]} ),
        .I_BLANK        ( vga_blank_s ),
        .I_HSYNC        ( vga_hs_s ),
        .I_VSYNC        ( vga_vs_s ),
        
        //-- PCM audio
        .I_AUDIO_ENABLE ( 1'b1 ),
        .I_AUDIO_PCM_L  ( sound_hdmi_s ),
        .I_AUDIO_PCM_R  ( sound_hdmi_s ),
        
        //-- TMDS parallel pixel synchronous outputs (serialize LSB first)
        .O_RED          ( tdms_r_s ),
        .O_GREEN        ( tdms_g_s ),
        .O_BLUE         ( tdms_b_s )
    );
    
    hdmi_out_altera hdmio
    (
        .clock_pixel_i  ( clk_pixel ),
        .clock_tdms_i   ( clk_dvi ),
        .red_i          ( tdms_r_s ),
        .green_i        ( tdms_g_s ),
        .blue_i         ( tdms_b_s ),
        .tmds_out_p     ( hdmi_p_s ),
        .tmds_out_n     ( hdmi_n_s )
    );

    wire [15:0] pcm_s;
    audio_pcm
    #( .CLK_RATE (40000000))
    audio_pcm
    (
        .clk         ( clk_pixel ),
        .sample_rate ( 0 ), //48khz
        .left_in     ( {audio, 6'b000000} ),
        .pcm_l       ( pcm_s ),
    );
 
   // assign sound_hdmi_s =  {1'b0, ~pcm_s[15], pcm_s[14:1]}; 
   //  assign sound_hdmi_s =  {1'b0, audio, 4'b0000}; 
      assign sound_hdmi_s =  {16'd0}; 
    
    assign tmds_o[7] = hdmi_p_s[2]; //-- 2+     
    assign tmds_o[6] = hdmi_n_s[2]; //-- 2-     
    assign tmds_o[5] = hdmi_p_s[1]; //-- 1+         
    assign tmds_o[4] = hdmi_n_s[1]; //-- 1-     
    assign tmds_o[3] = hdmi_p_s[0]; //-- 0+     
    assign tmds_o[2] = hdmi_n_s[0]; //-- 0- 
    assign tmds_o[1] = hdmi_p_s[3]; //-- CLK+   
    assign tmds_o[0] = hdmi_n_s[3]; //-- CLK-   

//--------- ROMS in SRAM ----------------------------------------------------
reg rom_oe, sound_oe;

always @(*)
begin
    

    if (rom_oe) rom_dout_s <= sram_data_io;
    else if (sound_oe) explosion_data_s <= sram_data_io;
    else gfx_data_s <= sram_data_io; 
end 

//PRG ROM 0000-3FFF
//GFX ROM 5000-5FFF

always @(posedge clk_24)
begin                                                           
    sram_addr_o  <= (ioctl_wr) ? ioctl_addr[18:0] : (rom_cs_s) ? {6'b00000, rom_addr_s[13:0] } : (clk_6) ? {6'b000011,explosion_addr_s } : {7'b0000101,gfx_addr_s };    
    sram_data_io <= (ioctl_wr) ? ioctl_dout : 8'hZZ;
    sram_we_n_o  <= ~ioctl_wr;

    sound_oe <= clk_6;
    rom_oe <= rom_cs_s; //delay the clock signal one cycle to make the OE for video data
end


endmodule
