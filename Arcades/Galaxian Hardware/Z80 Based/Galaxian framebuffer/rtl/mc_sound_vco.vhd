--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
--------------------------------------------------------------------------------
---- FPGA VCO
--------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use ieee.numeric_std.all;

use work.sine_package.all;

-- O_CLK = (I_CLK / 2^20) * I_STEP
entity MC_SOUND_VCO is
	port(
		I_CLK     : in  std_logic;
		I_RSTn    : in  std_logic;
		I_FS      : in  std_logic;
		I_STEP    : in  std_logic_vector( 7 downto 0);
		O_WAV     : out std_logic_vector( 7 downto 0)
	);
end;

architecture RTL of MC_SOUND_VCO is
	signal VCO1_CTR   : std_logic_vector(19 downto 0) := (others => '0');
	signal sine 		: std_logic_vector(14 downto 0) := (others => '0');

begin
	O_WAV <= sine(14 downto 7);
	process(I_CLK, I_RSTn)
	begin
		if (I_RSTn = '0') then
		  VCO1_CTR <= (others=>'0');
		elsif rising_edge(I_CLK) then
			if I_FS = '1' then
				VCO1_CTR <= VCO1_CTR + I_STEP;
				case VCO1_CTR(19 downto 18) is
					when "00" => 
						sine <= "100000000000000" + std_logic_vector( to_signed(get_table_value(     VCO1_CTR(17 downto 11)), 15));
					when "01" =>
						sine <= "100000000000000" + std_logic_vector( to_signed(get_table_value( not VCO1_CTR(17 downto 11)), 15));
					when "10" =>
						sine <= "100000000000000" + std_logic_vector(-to_signed(get_table_value(     VCO1_CTR(17 downto 11)), 15));
					when "11" =>
						sine <= "100000000000000" + std_logic_vector(-to_signed(get_table_value( not VCO1_CTR(17 downto 11)), 15));
					when others => null;
				end case;
			end if;
		end if;
	end process;
end RTL;
