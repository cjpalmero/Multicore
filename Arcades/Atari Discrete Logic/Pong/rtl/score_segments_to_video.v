/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*
  MIT License

  Copyright (c) 2019 Richard Eng

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

/*
  Pong - Score Segments to Video Circuit
  --------------------------------------
*/
`default_nettype none

module score_segments_to_video
(
    input wire h4, h8, h16, v4, v8, v16,
    input wire a, b, c, d, e, f, g,
    output wire score
);

wire c3d_out;
ls00 c3d(h4, h8, c3d_out);

wire e4b_out;
ls04 e4b(h16, e4b_out);

wire e2a_out;
ls10 e2a(v4, v8, h16, e2a_out);

wire e4a_out;
ls04 e4a(e2a_out, e4a_out);

wire e4c_out;
ls04 e4c(v16, e4c_out);

wire e5c_out;
ls27 e5c(e4b_out, h4, h8, e5c_out);

wire d2b_out;
ls02 d2b(c3d_out, e4b_out, d2b_out);

wire e5b_out;
ls27 e5b(v8, v4, e4b_out, e5b_out);

wire d4a_out;
ls10 d4a(e4c_out, f, e5c_out, d4a_out);

wire d5c_out;
ls10 d5c(e, v16, e5c_out, d5c_out);

wire c4c_out;
ls10 c4c(d2b_out, e4c_out, b, c4c_out);

wire d5a_out;
ls10 d5a(d2b_out, c, v16, d5a_out);

wire d4c_out;
ls10 d4c(a, e4c_out, e5b_out, d4c_out);

wire d4b_out;
ls10 d4b(g, e4a_out, e4c_out, d4b_out);

wire d5b_out;
ls10 d5b(e4a_out, v16, d, d5b_out);

ls30 d3(d4a_out, d5c_out, c4c_out, d5a_out, 1'b1, d4c_out, d4b_out, d5b_out, score);

endmodule
