/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*  This file is part of JTCPS1.
    JTCPS1 program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JTCPS1 program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JTCPS1.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 13-1-2020 */
    
`timescale 1ns/1ps

module jtcps1_timing(
    input              rst,
    input              clk,
    input              cen8,

    output reg [ 8:0]  hdump,
    output reg [ 8:0]  vdump,
    output reg [ 8:0]  vrender,
    output reg [ 8:0]  vrender1,
    output reg         start,
    // to video output
    output reg         HS,
    output reg         VS,
    output reg         VB,
    output             preVB,
    output reg         HB
);

reg [1:0] shVB;

`ifdef SIMULATION
initial begin
    hdump     = 9'd0;
    vrender1  = 8'd2;
    vrender   = 8'd1;
    vdump     = 9'd0;
    HS        = 1'b0;
    VS        = 1'b0;
    HB        = 1'b1;
    VB        = 1'b1;
    shVB      = 2'b11;
    start     = 1'b1;
end
`endif

assign preVB = shVB[0];

localparam [8:0] VS_START = 9'd251; // larger values pull it up
localparam [8:0] VS_END   = VS_START + 9'd4;
localparam [8:0] HS_START = 9'd487; // larger values pull it left
localparam [8:0] HS_LEN   = 9'd38;
localparam [8:0] HS_END   = HS_START<(9'd511-HS_LEN) ? (HS_START+HS_LEN) : ( HS_LEN-(9'd511-HS_START)-9'd1  );

always @(posedge clk) if(cen8) begin
    hdump     <= hdump+9'd1;
    //if ( vdump>=9'hf8  ) VB <= 1'b1;
    //if ( vdump==9'h0F  ) VB <= 1'b0;
    shVB[0] <= vdump<(9'd14) || vdump>9'd237; // 224 visible lines
    HB      <= hdump>=(9'd384+9'd64) || hdump<9'd64;
    // original HS reported to last for 36 clock ticks
    if( hdump== HS_START ) begin
        HS <= 1'b1;
        // VS must occur synchronized with HS for better compatibility
        // 250/261 wavy
        // 250/255 best
        // h100/h001 old
        if ( vdump==VS_START ) VS <= 1'b1;
        if ( vdump==VS_END   ) VS <= 1'b0;
    end
    if( hdump== HS_END ) HS <= 1'b0;
    start  <= hdump==9'h1ff;
    if(&hdump) begin
        hdump   <= 9'd0;
        vrender1<= vrender1==9'd261 ? 9'd0 : vrender1+9'd1;
        vrender <= vrender1;
        vdump   <= vrender;
        { VB, shVB[1] } <= shVB;
    end
end

endmodule