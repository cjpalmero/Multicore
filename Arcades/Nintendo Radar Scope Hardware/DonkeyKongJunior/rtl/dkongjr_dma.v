/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
// Sprite DMA.
// 
// Author: gaz68 (https://github.com/gaz68)
// October 2019
//
// Simplified sprite DMA. To Do: Implement full 8257 DMA controller.
//============================================================================

module dkongjr_dma
(
	input		I_CLK,
	input		I_RSTn,
	input		I_DMA_TRIG,
	input		[7:0]I_DMA_DS,

	output	[9:0]O_DMA_AS,
	output 	[9:0]O_DMA_AD,
	output	[7:0]O_DMA_DD,
	output	O_DMA_CES,
	output	O_DMA_CED
);

parameter dma_cnt_end = 10'h17F;

reg W_DMA_EN = 1'b0;
reg [10:0]W_DMA_CNT;
reg [7:0]W_DMA_DATA;
reg [9:0]DMA_ASr;
reg [9:0]DMA_ADr;
reg [7:0]DMA_DDr;
reg DMA_CESr, DMA_CEDr;

always @(posedge I_CLK)
begin
	reg old_trig;

	old_trig <= I_DMA_TRIG;

	if(~old_trig & I_DMA_TRIG)
		begin
			DMA_ASr		<= 10'h100; 
			DMA_ADr		<= 0;
			W_DMA_CNT	<= 0;
			W_DMA_EN		<= 1'b1;
			DMA_CESr		<= 1'b1;
			DMA_CEDr		<= 1'b1;
		end
	else if(W_DMA_EN == 1'b1)
		begin
			case(W_DMA_CNT[1:0])
				1: DMA_DDr <= I_DMA_DS;
				2: DMA_ASr <= DMA_ASr + 1'd1;
				3: DMA_ADr <= DMA_ADr + 1'd1;
				default:;
			endcase 
			W_DMA_CNT <= W_DMA_CNT + 1'd1;
			W_DMA_EN <= W_DMA_CNT==dma_cnt_end*4 ? 1'b0 : 1'b1;
		end
	else
		begin
			DMA_CESr <= 1'b0;
			DMA_CEDr <= 1'b0;
		end
end

assign O_DMA_AS	= DMA_ASr;
assign O_DMA_AD   = DMA_ADr;
assign O_DMA_DD   = DMA_DDr;
assign O_DMA_CES  = DMA_CESr;
assign O_DMA_CED  = DMA_CEDr;


endmodule
