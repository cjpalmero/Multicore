--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
---------------------------------------------------------------------------------
-- Usb joystick decoder by Dar (darfpga@aol.fr)
-- http://darfpga.blogspot.fr
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

library work;
use work.usb_report_pkg.all;

entity usb_joystick_decoder is
port(
 clk     : in std_logic;
 
 usb_report : in usb_report_t;
 new_usb_report : in std_logic;
 
 joyBCPPFRLDU : inout std_logic_vector (8 downto 0)

);
end usb_joystick_decoder;

architecture struct of usb_joystick_decoder is

signal data : std_logic_vector(8 downto 0) := '0'&X"AA";

begin

joystick_decoder : process (clk)
	variable byte_cnt : integer range 0 to 9;
begin 
	if rising_edge(clk) then
--		joyBCPPFRLDU <= (others => '0');
		if new_usb_report = '1' then
			joyBCPPFRLDU(0) <= usb_report(3)(0);  -- #2 0x01 (up)      dir. cross up
			joyBCPPFRLDU(1) <= usb_report(3)(1);  -- #2 0x02 (down)    dir. cross low
			joyBCPPFRLDU(2) <= usb_report(3)(2);  -- #2 0x04 (left)    dir. cross left
			joyBCPPFRLDU(3) <= usb_report(3)(3);  -- #2 0x08 (right)   dir. cross right
			joyBCPPFRLDU(4) <= usb_report(4)(4);  -- #3 0x10 (fire)    btn low
			joyBCPPFRLDU(5) <= usb_report(4)(0);  -- #3 0x01 (start 1) trigger left
			joyBCPPFRLDU(6) <= usb_report(4)(1);  -- #3 0x02 (start 2) trigger right
			joyBCPPFRLDU(7) <= usb_report(4)(7);  -- #3 0x80 (coin)    btn up
			joyBCPPFRLDU(8) <= usb_report(4)(6);  -- #3 0x40 (bomb)    btn left		
		end if;
	end if;
end process;

end struct;