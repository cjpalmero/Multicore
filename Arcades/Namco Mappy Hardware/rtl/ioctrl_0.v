/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///------------------------------------------
//	 I/O Chip for "Mappy/Druaga/DigDug2"
//
//          Copyright (c) 2007 MiSTer-X
//------------------------------------------
// TODO: DSW2 = DIPSW[23:16]

		case ( mema[4'h8] )

		4'h1: begin
			mema[4'h0] <= 0;
			mema[4'h1] <= 0;
			mema[4'h2] <= 0;
			mema[4'h3] <= 0;
		end

		4'h3: begin
			credit_add = 0;
			credit_sub = 0;

			if ( iCSTART12[2] & ( credits < 99 ) ) begin
				credit_add = 8'h01;
				credits = credits + 1'd1;
			end
	
			if ( mema[4'h9] == 0 ) begin
				if ( ( credits >= 2 ) & iCSTART12[1] ) begin
					credit_sub = 8'h02;
					credits = credits - 2'd1;
				end else if ( ( credits >= 1 ) & iCSTART12[0] ) begin
					credit_sub = 8'h01;
					credits = credits - 1'd1;
				end
			end

			mema[4'h0] <= credit_add;
			mema[4'h1] <= credit_sub;
			mema[4'h2] <= CREDIT_TENS;
			mema[4'h3] <= CREDIT_ONES;
			mema[4'h4] <= STKTRG12[3:0]; 
			mema[4'h5] <= { CSTART12[0], iCSTART12[0], STKTRG12[4], iSTKTRG12[4] };
			mema[4'h6] <= STKTRG12[9:6];				
			mema[4'h7] <= { CSTART12[1], iCSTART12[1], STKTRG12[10], iSTKTRG12[10] };
		end
	
		4'h4: begin
			mema[4'h0] <= 0;
			mema[4'h1] <= 0;
			mema[4'h2] <= 0;
			mema[4'h3] <= 0;
			mema[4'h4] <= 0;
			mema[4'h5] <= 0;
			mema[4'h6] <= { CSTART12[1], CSTART12[0], 2'b00 };
			mema[4'h7] <= { CSTART12[1], CSTART12[0], 2'b00 };
		end

		4'h5: begin
			mema[4'h0] <= 4'h0;
			mema[4'h1] <= 4'h8; 
			mema[4'h2] <= 4'h4; 
			mema[4'h3] <= 4'h6; 
			mema[4'h4] <= 4'hE; 
			mema[4'h5] <= 4'hD; 
			mema[4'h6] <= 4'h9; 
			mema[4'h7] <= 4'hD; 
		end

		default: begin end
	
		endcase


		case ( memb[4'h8] )
	
		4'h1: begin
			memb[4'h0] <= 0;
			memb[4'h1] <= 0;
			memb[4'h2] <= 0;
			memb[4'h3] <= 0;
		end
	
		4'h3: begin
			memb[4'h0] <= 0;
			memb[4'h1] <= 0;
			memb[4'h2] <= 0;
			memb[4'h3] <= 0;
			memb[4'h4] <= 0;
			memb[4'h5] <= 0;
			memb[4'h6] <= 0;
			memb[4'h7] <= 0;
		end
	
		4'h4: begin
			memb[4'h0] <= DIPSW[11:8];
			memb[4'h1] <= DIPSW[3:0];
			memb[4'h2] <= DIPSW[7:4];
			memb[4'h4] <= DIPSW[15:12];
			memb[4'h6] <= DIPSW[7:4];

			memb[4'h3] <= 0;
			memb[4'h5] <= { DIPSW[3:2], STKTRG12[ 5], iSTKTRG12[ 5] };
			memb[4'h7] <= {      2'b00, STKTRG12[11], iSTKTRG12[11] };
		end

		4'h5: begin
			memb[4'h0] <= 4'h0;
			memb[4'h1] <= 4'h8; 
			memb[4'h2] <= 4'h4; 
			memb[4'h3] <= 4'h6; 
			memb[4'h4] <= 4'hE; 
			memb[4'h5] <= 4'hD; 
			memb[4'h6] <= 4'h9; 
			memb[4'h7] <= 4'hD; 
		end

		default: begin end

		endcase

