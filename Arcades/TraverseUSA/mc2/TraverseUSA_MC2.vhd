--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
---------------------------------------------------------------------------------
-- Multicore 2 Top level for TraverseUSA 
-- Victor Trucco OCT/2019
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
-- Traverse USA by Dar (darfpga@aol.fr) (16/03/2019)
-- http://darfpga.blogspot.fr
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.ALL;
use ieee.numeric_std.all;

entity TraverseUSA_MC2 is
port(
 -- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);

		-- SRAMs (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_oe_n_o			: out   std_logic								:= '1';
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= 'Z';
		sd_sclk_o			: out   std_logic								:= 'Z';
		sd_mosi_o			: out   std_logic								:= 'Z';
		sd_miso_i			: in    std_logic								:= 'Z';

		-- Joysticks
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p9_i			: in    std_logic;
		joyX_p7_o			: out   std_logic								:= '1';

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
		tmds_o				: out   std_logic_vector(7 downto 0)	:= (others => '0');

		--STM32
		stm_rx_o				: out std_logic		:= 'Z'; -- stm RX pin, so, is OUT on the slave
		stm_tx_i				: in  std_logic		:= 'Z'; -- stm TX pin, so, is IN on the slave
		stm_rst_o			: out std_logic		:= 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
		
		stm_a15_io			: inout std_logic;
		stm_b8_io			: inout std_logic		:= 'Z';
		stm_b9_io			: inout std_logic		:= 'Z';
		stm_b12_io			: inout std_logic		:= 'Z';
		stm_b13_io			: inout std_logic		:= 'Z';
		stm_b14_io			: inout std_logic		:= 'Z';
		stm_b15_io			: inout std_logic		:= 'Z'
);
end TraverseUSA_MC2;

architecture struct of TraverseUSA_MC2 is

	function to_slv(s: string) return std_logic_vector is 
        constant ss: string(1 to s'length) := s; 
        variable answer: std_logic_vector(1 to 8 * s'length); 
        variable p: integer; 
        variable c: integer; 
    begin 
        for i in ss'range loop
            p := 8 * i;
            c := character'pos(ss(i));
            answer(p - 7 to p) := std_logic_vector(to_unsigned(c,8)); 
        end loop; 
        return answer; 
    end function; 
	 
--	 component sdram is 
--		port(
--		init: in std_logic;          							-- reset to initialize RAM
--		clk: in std_logic;          							-- clock ~100MHz
--																		--
--																		-- SDRAM_* - signals to the MT48LC16M16 chip
--		SDRAM_DQ: out std_logic_vector(15 downto 0);    -- 16 bit bidirectional data bus
--		SDRAM_A : out std_logic_vector(12 downto 0);    -- 13 bit multiplexed address bus
--		SDRAM_DQML : out std_logic;  							-- two byte masks
--		SDRAM_DQMH : out std_logic;  							-- 
--		SDRAM_BA : out std_logic_vector(1 downto 0);    -- two banks
--		SDRAM_nCS : out std_logic;   							-- a single chip select
--		SDRAM_nWE : out std_logic;   							-- write enable
--		SDRAM_nRAS : out std_logic;  							-- row address select
--		SDRAM_nCAS : out std_logic;  							-- columns address select
--		SDRAM_CKE : out std_logic;   							-- clock enable
--						 --
--		wtbt: in std_logic_vector(1 downto 0);       	-- 16bit mode:  bit1 - write high byte, bit0 - write low byte,
--																		-- 8bit mode:  2'b00 - use addr[0] to decide which byte to write
--																		-- Ignored while reading.
--																		--
--		addr: in std_logic_vector(24 downto 0);         -- 25 bit address for 8bit mode. addr[0] = 0 for 16bit mode for correct operations.
--		dout: out std_logic_vector(15 downto 0);        -- data output to cpu
--		din: in std_logic_vector(15 downto 0);          -- data input from cpu
--		we: in std_logic;           							-- cpu requests write
--		rd: in std_logic;           							-- cpu requests read
--		ready : out std_logic       							-- dout is valid. Ready to accept new read/write.
--	);
--	end component;

	component scandoubler is
	port
	(
		--// system interface
		clk_sys : in std_logic;

		--// scanlines (00-none 01-25% 10-50% 11-75%)
		scanlines : in std_logic_vector(1 downto 0);
		ce_divider : in std_logic;

		--// shifter video interface
		hs_in		: in std_logic;
		vs_in		: in std_logic;
		r_in 		: in std_logic_vector(5 downto 0);
		g_in 		: in std_logic_vector(5 downto 0);
		b_in 		: in std_logic_vector(5 downto 0);
		
		--// output interface
		hs_out	: out std_logic;
		vs_out	: out std_logic;
		r_out 	: out std_logic_vector(5 downto 0);
		g_out 	: out std_logic_vector(5 downto 0);
		b_out 	: out std_logic_vector(5 downto 0)
	);
	end component;
	 
 signal clk_sys  : std_logic;
 signal clk_aud  : std_logic;
  signal pll_locked  : std_logic;

 signal r         : std_logic_vector(1 downto 0);
 signal g         : std_logic_vector(2 downto 0);
 signal b         : std_logic_vector(2 downto 0);
 signal video_clk : std_logic;
 signal csync     : std_logic;
 signal blankn    : std_logic;
 
 signal audio        : std_logic_vector(10 downto 0);
 signal pwm_accumulator : std_logic_vector(12 downto 0);
 signal sound_string : std_logic_vector(31 downto 0);
 signal reset        : std_logic;
 
 signal  reset_n      : std_logic;
 
 signal kbd_intr      : std_logic;
 signal kbd_scancode  : std_logic_vector(7 downto 0);
 signal joyBCPPFRLDU   : std_logic_vector(8 downto 0);
 
 -- SRAM
	signal rom_data_s : std_logic_vector(7 downto 0);
	signal sram_addr_s : std_logic_vector(18 downto 0);	
	signal sram_data_s : std_logic_vector(7 downto 0);
	signal sram_we_n_s : std_logic := '1';
 
 -- Video
	signal video_r_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video_g_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video_b_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video_hsync_n_s		: std_logic								:= '1';
	signal video_vsync_n_s		: std_logic								:= '1';
	signal vga_hsync_n_s		: std_logic								:= '1';
	signal vga_vsync_n_s		: std_logic								:= '1';
	
	
	 -- OSD
	 signal pump_active_s 	 : std_logic := '0';
	 signal osd_s  		 : std_logic_vector(7 downto 0) := "00111111";
	 signal clock_div_q	: unsigned(7 downto 0) 				:= (others => '0');
	 signal keys_s			: std_logic_vector( 7 downto 0) := (others => '1');	
	 signal power_on_reset     : std_logic := '0';
	 
	 
	 -- joysticks
	 signal joy1_s : std_logic_vector(11 downto 0);
	 signal joy2_s : std_logic_vector(11 downto 0);
	 signal joyP7_s : std_logic;
	 
	 
	  -- HDMI
	signal clk_vga : std_logic;
	signal clk_dvi : std_logic;
	signal clk_dvi_180 : std_logic;
	
	signal video15_r_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video15_g_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal video15_b_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	
	signal genlock_r_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal genlock_g_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal genlock_b_s				: std_logic_vector(4 downto 0)	:= (others => '0');
	signal genlock_hs_s				: std_logic;
	signal genlock_vs_s				: std_logic;
	signal genlock_blank_s			: std_logic;
		
	signal pcm_audio_s				: std_logic_vector(4 downto 0);
	
	signal tdms_r_s					: std_logic_vector( 9 downto 0);
	signal tdms_g_s					: std_logic_vector( 9 downto 0);
	signal tdms_b_s					: std_logic_vector( 9 downto 0);
	signal hdmi_p_s					: std_logic_vector( 3 downto 0);
	signal hdmi_n_s					: std_logic_vector( 3 downto 0);
	
	-- 
		
	signal sdram_addr_s : std_logic_vector( 3 downto 0);
		
		
	signal cart_addr: std_logic_vector( 14 downto 0);
	signal sdram_do: std_logic_vector( 15 downto 0);


	--//Coinage_B(7-4) / Cont. play(3) / Fuel consumption(2) / Fuel lost when collision (1-0)
	signal dip1 : std_logic_vector (7 downto 0) := X"ff";
	--//Diag(7) / Demo(6) / Zippy(5) / Freeze (4) / M-Km(3) / Coin mode (2) / Cocktail(1) / Flip(0)
	signal dip2 : std_logic_vector (7 downto 0) := "11110101";
	
	signal btn_one_player 	: std_logic := '0'; 
	signal btn_two_players 	: std_logic := '0'; 
	signal btn_left 			: std_logic := '0'; 
	signal btn_right 			: std_logic := '0'; 
	signal btn_down 			: std_logic := '0'; 
	signal btn_up 				: std_logic := '0'; 
	signal btn_fire1 			: std_logic := '0'; 
	signal btn_fire2 			: std_logic := '0'; 
	signal btn_fire3 			: std_logic := '0'; 
	signal btn_coin  			: std_logic := '0'; 

	
	signal dac_s 	: std_logic;
	signal video_ena_s :std_logic;
	----
	

	begin


reset_n <= btn_n_i(3) or btn_n_i(4);
--reset <= power_on_reset or pump_active_s or (not btn_n_i(2));
reset <= pump_active_s or power_on_reset or (not btn_n_i(4));


pll : entity work.pll
port map(
 inclk0 => clock_50_i,
 areset => '0',
 c0 => clk_sys,
 c1 => clk_aud,
  c2 => clk_vga,
 c3 => clk_dvi,
 c4 => clk_dvi_180,
 locked => pll_locked
);

traverse_usa : work.traverse_usa 
port map (
	clock_36     => clk_sys,         
	clock_0p895  => clk_aud,         
	reset        => reset, 				

	video_r      => r,               
	video_g      => g,               
	video_b      => b,               
   video_hs     => video_hsync_n_s,              
	video_vs     => video_vsync_n_s,              
	video_blankn => blankn  ,   
	video_ena 	 => video_ena_s,	

	audio_out    => audio,           
	
	cpu_rom_addr => cart_addr,       
	cpu_rom_do   => rom_data_s   ,
	cpu_rom_rd   => open         ,
	
	dip_switch_1 => dip1            ,  
	dip_switch_2 => dip2            ,
	
	start2       => btn_two_players ,
	start1       => btn_one_player  ,
	coin1        => btn_coin        ,

	right1       => btn_right         ,
	left1        => btn_left          ,
	brake1       => btn_down          ,
	accel1       => btn_up            ,

	right2       => btn_right         ,
	left2        => btn_left          ,
	brake2       => btn_down          ,
	accel2       => btn_up            
	);

	video_r_s <= r & "000" when blankn = '1' else (others=>'0');
	video_g_s <= g & "00"  when blankn = '1' else (others=>'0');
	video_b_s <= b & "00"  when blankn = '1' else (others=>'0');
	
	--vga_r_o <= video_r_s;
	--vga_g_o <= video_g_s;
	--vga_b_o <= video_b_s;
	--vga_hsync_n_o	<= video_hsync_n_s;
	--vga_vsync_n_o	<= video_vsync_n_s;
	
 scandoubler1 : scandoubler
	port map 
	(
		--// system interface
		clk_sys => clk_sys,

		--// scanlines (00-none 01-25% 10-50% 11-75%)
		scanlines => "00",
		ce_divider => '0',

		--// shifter video interface
		hs_in => video_hsync_n_s,
		vs_in => video_vsync_n_s,
		r_in  => video_r_s&'0',
		g_in  => video_g_s&'0',
		b_in  => video_b_s&'0',

		--// output interface
		hs_out => vga_hsync_n_o,
		vs_out => vga_vsync_n_o,
		r_out(5 downto 1)  => vga_r_o,
		g_out(5 downto 1)  => vga_g_o,
		b_out(5 downto 1)  => vga_b_o
	);
	

	dac :  work.dac
	generic map(
		C_bits => 11)
	port map(
		clk_i => clk_aud,
		res_n_i => '1',
		dac_i => audio,
		dac_o	=> dac_s
		);

		dac_l_o <= dac_s;
		dac_r_o <= dac_s;
		
	btn_two_players <= (not btn_n_i(2))or joyBCPPFRLDU(7);
	btn_one_player  <= (not btn_n_i(1))or joyBCPPFRLDU(6);
	btn_coin			 <= (not btn_n_i(3))or joyBCPPFRLDU(5);
	
	btn_right       <= (not joy1_s(3)) or joyBCPPFRLDU(3);
	btn_left        <= (not joy1_s(2)) or joyBCPPFRLDU(2);
	btn_down        <= (not joy1_s(1)) or joyBCPPFRLDU(1);
	btn_up          <= (not joy1_s(0)) or joyBCPPFRLDU(0);


--- Joystick read with sega 6 button support----------------------

	process(vga_hsync_n_s)
		variable state_v : unsigned(7 downto 0) := (others=>'0');
		variable j1_sixbutton_v : std_logic := '0';
		variable j2_sixbutton_v : std_logic := '0';
	begin
		if falling_edge(vga_hsync_n_s) then
		
			state_v := state_v + 1;
			
			case state_v is
				-- joy_s format MXYZ SACB RLDU
			
				when X"00" =>  
					joyP7_s <= '0';
					
				when X"01" =>
					joyP7_s <= '1';

				when X"02" => 
					joy1_s(3 downto 0) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- R, L, D, U
					joy2_s(3 downto 0) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- R, L, D, U
					joy1_s(5 downto 4) <= joy1_p9_i & joy1_p6_i; -- C, B
					joy2_s(5 downto 4) <= joy2_p9_i & joy2_p6_i; -- C, B					
					joyP7_s <= '0';
					j1_sixbutton_v := '0'; -- Assume it's not a six-button controller
					j2_sixbutton_v := '0'; -- Assume it's not a six-button controller

				when X"03" =>
					if joy1_right_i = '0' and joy1_left_i = '0' then -- it's a megadrive controller
								joy1_s(7 downto 6) <= joy1_p9_i & joy1_p6_i; -- Start, A
					else
								joy1_s(7 downto 4) <= '1' & joy1_p9_i & '1'  & joy1_p6_i; -- read A/B as master System
					end if;
							
					if joy2_right_i = '0' and joy2_left_i = '0' then -- it's a megadrive controller
								joy2_s(7 downto 6) <=  joy2_p9_i & joy2_p6_i; -- Start, A
					else
								joy2_s(7 downto 4) <= '1' & joy2_p9_i & '1' & joy2_p6_i; -- read A/B as master System
					end if;
					
										
					joyP7_s <= '1';
			
				when X"04" =>  
					joyP7_s <= '0';

				when X"05" =>
					if joy1_right_i = '0' and joy1_left_i = '0' and joy1_down_i = '0' and joy1_up_i = '0' then 
						j1_sixbutton_v := '1'; --it's a six button
					end if;
					
					if joy2_right_i = '0' and joy2_left_i = '0' and joy2_down_i = '0' and joy2_up_i = '0' then 
						j2_sixbutton_v := '1'; --it's a six button
					end if;
					
					joyP7_s <= '1';
					
				when X"06" =>
					if j1_sixbutton_v = '1' then
						joy1_s(11 downto 8) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- Mode, X, Y e Z
					end if;
					
					if j2_sixbutton_v = '1' then
						joy2_s(11 downto 8) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- Mode, X, Y e Z
					end if;
					
					joyP7_s <= '0';

				when others =>
					joyP7_s <= '1';
					
			end case;

		end if;
	end process;
	
	joyX_p7_o <= joyP7_s;
---------------------------






-- get scancode from keyboard
keyboard : entity work.io_ps2_keyboard
port map (
  clk       => clk_sys,
  kbd_clk   => ps2_clk_io,
  kbd_dat   => ps2_data_io,
  interrupt => kbd_intr,
  scancode  => kbd_scancode
);

-- translate scancode to joystick
joystick : entity work.kbd_joystick
port map (
  clk         => clk_sys,
  kbdint      => kbd_intr,
  kbdscancode => std_logic_vector(kbd_scancode), 
  joyBCPPFRLDU  => joyBCPPFRLDU
);





OSB_BLOCK: block 

	type config_array is array(natural range 15 downto 0) of std_logic_vector(7 downto 0);

		component osd is
		generic
		(
			STRLEN 		 : integer := 0;
			OSD_X_OFFSET : std_logic_vector(9 downto 0) := (others=>'0');
			OSD_Y_OFFSET : std_logic_vector(9 downto 0) := (others=>'0');
			OSD_COLOR    : std_logic_vector(2 downto 0) := (others=>'0')
		);
		port
		(
			-- OSDs pixel clock, should be synchronous to cores pixel clock to
			-- avoid jitter.
			pclk		: in std_logic;

			-- SPI interface
			sck		: in std_logic;
			ss			: in std_logic;
			sdi		: in std_logic;
			sdo		: out std_logic;

			-- VGA signals coming from core
			red_in 	: in std_logic_vector(4 downto 0);
			green_in : in std_logic_vector(4 downto 0);
			blue_in 	: in std_logic_vector(4 downto 0);
			hs_in		: in std_logic;
			vs_in		: in std_logic;
			
			-- VGA signals going to video connector
			red_out	: out std_logic_vector(4 downto 0);
			green_out: out std_logic_vector(4 downto 0);
			blue_out	: out std_logic_vector(4 downto 0);
			hs_out 	: out std_logic;
			vs_out 	: out std_logic;
			
			-- external data in to the microcontroller
			data_in 	: in std_logic_vector(7 downto 0);
			conf_str : in std_logic_vector( (STRLEN * 8)-1 downto 0);
			
			-- data pump to sram
			pump_active_o	: out std_logic := '0';
			sram_a_o 		: out std_logic_vector(18 downto 0);
			sram_d_o 		: out std_logic_vector(7 downto 0);
			sram_we_n_o 	: out std_logic := '1';
			
			config_buffer_o: out config_array
		);
		end component;
		
		alias SPI_DI  : std_logic is stm_b15_io;
		alias SPI_DO  : std_logic is stm_b14_io;
		alias SPI_SCK : std_logic is stm_b13_io;
		alias SPI_SS3 : std_logic is stm_b12_io;
		
		signal vga_r_out_s : std_logic_vector(3 downto 0);
		signal vga_g_out_s : std_logic_vector(3 downto 0);
		signal vga_b_out_s : std_logic_vector(3 downto 0);
		
		signal sram_addr_s : std_logic_vector(18 downto 0) := (others=>'1');
		signal sram_data_s : std_logic_vector(7 downto 0);
		signal sram_we_s 	 : std_logic := '1';
		
		signal power_on_s		: std_logic_vector(15 downto 0)	:= (others => '1');
		
		signal config_buffer_s : config_array;

		-- config string
		constant STRLEN		: integer := 14;
		constant CONF_STR		: std_logic_vector((STRLEN * 8)-1 downto 0) := to_slv("P,Traverse.dat");
		
	begin
		
		
		osd1 : osd 
		generic map
		(
			STRLEN => STRLEN,
			OSD_COLOR => "001", -- RGB
			OSD_X_OFFSET => "0000010010", -- 50
			OSD_Y_OFFSET => "0000001111"  -- 15
		)
		port map
		(
			pclk       => clk_sys,

			-- spi for OSD
			sdi        => SPI_DI,
			sck        => SPI_SCK,
			ss         => SPI_SS3,
			sdo        => SPI_DO,
			
			red_in     => video_r_s,
			green_in   => video_g_s,
			blue_in    => video_b_s,
			hs_in      => video_hsync_n_s,
			vs_in      => video_vsync_n_s,

			red_out    => video15_r_s,
			green_out  => video15_g_s,
			blue_out   => video15_b_s,
			hs_out     => vga_hsync_n_s,
			vs_out     => vga_vsync_n_s,

			data_in		=> osd_s,
			conf_str		=> CONF_STR,
						
			pump_active_o	=> pump_active_s,
			sram_a_o			=> sram_addr_s,
			sram_d_o			=> sram_data_s,
			sram_we_n_o		=> sram_we_n_s,
			config_buffer_o=> config_buffer_s		
		);
			

		
		sram_addr_o   <= sram_addr_s when pump_active_s = '1' else "0000" & cart_addr;
		sram_data_io  <= sram_data_s when pump_active_s = '1' else (others=>'Z');
		rom_data_s 	  <= sram_data_io;
		sram_oe_n_o   <= '0';
		sram_we_n_o   <= sram_we_n_s;


		--start the microcontroller OSD menu after the power on
		process (clk_sys, reset_n, osd_s)
		begin
			if rising_edge(clk_sys) then
				if reset_n = '0' then
					power_on_s <= (others=>'1');
				elsif power_on_s /= x"0000" then
					power_on_s <= power_on_s - 1;
					power_on_reset <= '1';
					osd_s <= "00111111";
				else
					power_on_reset <= '0';
					
				end if;
				
				if pump_active_s = '1' and osd_s <= "00111111" then
					osd_s <= "11111111";
				end if;
				
			end if;
		end process;

	end block;	
	

	



end struct;
