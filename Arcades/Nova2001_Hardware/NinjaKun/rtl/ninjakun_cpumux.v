/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/module ninjakun_cpumux
(
	input				MCLK,
	output [15:0]	CPADR,
	output  [7:0]	CPODT,
	input   [7:0]	CPIDT,
	output    		CPRED,
	output    		CPWRT,

	output reg		CP0CL,
	output reg		CP0CE_P,
	output reg		CP0CE_N,
	input  [15:0]	CP0AD,
	input   [7:0]	CP0OD,
	output  [7:0]	CP0ID,
	input    		CP0RD,
	input    		CP0WR,

	output reg		CP1CL,
	output reg		CP1CE_P,
	output reg		CP1CE_N,
	input  [15:0]	CP1AD,
	input   [7:0]	CP1OD,
	output  [7:0]	CP1ID,
	input    		CP1RD,
	input    		CP1WR
);

reg  [7:0] CP0DT, CP1DT;
reg  [3:0] PHASE;
reg		  CSIDE;
always @( posedge MCLK ) begin	// 48MHz
	CP0CE_P <= 0; CP0CE_N <= 0;
	CP1CE_P <= 0; CP1CE_N <= 0;
	case (PHASE)
	0: begin CP0DT <= CPIDT; CP0CE_P <= 1; CP1CE_N <= 1; end
	1: CSIDE <= 0;
	8: begin CP1DT <= CPIDT; CP1CE_P <= 1; CP0CE_N <= 1; end
	9: CSIDE <= 1;
	default:;
	endcase
end
always @( posedge MCLK ) begin
	case (PHASE)
	1: begin CP0CL <= 1; CP1CL <= 0; end
	9: begin CP1CL <= 1; CP0CL <= 0; end
	default:;
	endcase
	PHASE <= PHASE+1'd1;
end

assign CPADR = CSIDE ? CP1AD : CP0AD;
assign CPODT = CSIDE ? CP1OD : CP0OD;
assign CPRED = CSIDE ? CP1RD : CP0RD;
assign CPWRT = CSIDE ? CP1WR : CP0WR;
assign CP0ID = CSIDE ? CP0DT : CPIDT;
assign CP1ID = CSIDE ? CPIDT : CP1DT;

endmodule 