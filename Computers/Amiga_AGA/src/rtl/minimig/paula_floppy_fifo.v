/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///2048 words deep, 16 bits wide, fifo
//data is written into the fifo when wr=1
//reading is more or less asynchronous if you read during the rising edge of clk
//because the output data is updated at the falling edge of the clk
//when rd=1, the next data word is selected 


module paula_floppy_fifo
(
	input 	clk,		    	//bus clock
  input clk7_en,
	input 	reset,			   	//reset 
	input	[15:0] in,			//data in
	output	reg [15:0] out,	//data out
	input	rd,					//read from fifo
	input	wr,					//write to fifo
	output	reg empty,			//fifo is empty
	output	full,				//fifo is full
	output	[11:0] cnt       // number of entries in FIFO
);

//local signals and registers
reg 	[15:0] mem [2047:0];	// 2048 words by 16 bit wide fifo memory (for 2 MFM-encoded sectors)
reg		[11:0] in_ptr;			//fifo input pointer
reg		[11:0] out_ptr;			//fifo output pointer
wire	equal;					//lower 11 bits of in_ptr and out_ptr are equal


// count of FIFO entries
assign cnt = in_ptr - out_ptr;

//main fifo memory (implemented using synchronous block ram)
always @(posedge clk) begin
  if (clk7_en) begin
  	if (wr)
  		mem[in_ptr[10:0]] <= in;
  end
end

always @(posedge clk) begin
  if (clk7_en) begin
  	out=mem[out_ptr[10:0]];
  end
end

//fifo write pointer control
always @(posedge clk) begin
  if (clk7_en) begin
  	if (reset)
  		in_ptr[11:0] <= 0;
  	else if(wr)
  		in_ptr[11:0] <= in_ptr[11:0] + 12'd1;
  end
end

// fifo read pointer control
always @(posedge clk) begin
  if (clk7_en) begin
  	if (reset)
  		out_ptr[11:0] <= 0;
  	else if (rd)
  		out_ptr[11:0] <= out_ptr[11:0] + 12'd1;
  end
end

// check lower 11 bits of pointer to generate equal signal
assign equal = (in_ptr[10:0]==out_ptr[10:0]) ? 1'b1 : 1'b0;

// assign output flags, empty is delayed by one clock to handle ram delay
always @(posedge clk) begin
  if (clk7_en) begin
  	if (equal && (in_ptr[11]==out_ptr[11]))
  		empty <= 1'b1;
  	else
  		empty <= 1'b0;
  end
end
		
assign full = (equal && (in_ptr[11]!=out_ptr[11])) ? 1'b1 : 1'b0;	


endmodule

