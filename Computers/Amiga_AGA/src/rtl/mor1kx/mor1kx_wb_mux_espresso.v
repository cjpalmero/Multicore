/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: RF writeback mux for espresso pipeline

  Choose between ALU and LSU input. All combinatorial

  Copyright (C) 2012 Authors

  Author(s): Julius Baxter <juliusbaxter@gmail.com>

***************************************************************************** */

`include "mor1kx-defines.v"

module mor1kx_wb_mux_espresso
  (/*AUTOARG*/
   // Outputs
   rf_result_o,
   // Inputs
   clk, rst, alu_result_i, lsu_result_i, ppc_i, pc_fetch_next_i,
   spr_i, op_jal_i, op_lsu_load_i, op_mfspr_i
   );

   parameter OPTION_OPERAND_WIDTH = 32;

   input clk, rst;

   input [OPTION_OPERAND_WIDTH-1:0] alu_result_i;
   input [OPTION_OPERAND_WIDTH-1:0] lsu_result_i;
   input [OPTION_OPERAND_WIDTH-1:0] ppc_i;
   input [OPTION_OPERAND_WIDTH-1:0] pc_fetch_next_i;
   input [OPTION_OPERAND_WIDTH-1:0] spr_i;

   output [OPTION_OPERAND_WIDTH-1:0] rf_result_o;

   input 			      op_jal_i;
   input 			      op_lsu_load_i;
   input 			      op_mfspr_i;


   assign rf_result_o = op_lsu_load_i ? lsu_result_i :
			op_mfspr_i ? spr_i :
			// Use the PC we've calcuated from the fetch unit
			// to save inferring a 32-bit adder here like we
			// would if we did "ppc_i + 8"
			op_jal_i ? pc_fetch_next_i:
			alu_result_i;

endmodule // mor1kx_wb_mux_espresso
