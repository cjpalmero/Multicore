--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

library work;
use work.pace_pkg.all;
use work.target_pkg.all;
use work.video_controller_pkg.all;

package project_pkg is

	--  
	-- PACE constants which *MUST* be defined
	--
	
  -- Reference clock is 50MHz
	constant PACE_HAS_PLL								      : boolean := true;
  --constant PACE_HAS_FLASH                   : boolean := true;
  constant PACE_HAS_SRAM                    : boolean := true;
  constant PACE_HAS_SDRAM                   : boolean := false;
  constant PACE_HAS_SERIAL                  : boolean := false;
  
	constant PACE_JAMMA	                      : PACEJamma_t := PACE_JAMMA_NGC;

  constant PACE_VIDEO_CONTROLLER_TYPE       : PACEVideoController_t := PACE_VIDEO_NONE;
  constant PACE_CLK0_DIVIDE_BY              : natural := 7;
  constant PACE_CLK0_MULTIPLY_BY            : natural := 8;   -- 50*8/7 = 57M143Hz (57.27272)
  constant PACE_CLK1_DIVIDE_BY              : natural := 7;
  constant PACE_CLK1_MULTIPLY_BY            : natural := 8;  	-- 50*8/7 = 57M143Hz (57.27272)
	constant PACE_VIDEO_H_SCALE       	      : integer := 1;
	constant PACE_VIDEO_V_SCALE       	      : integer := 2;

  --constant PACE_HAS_OSD                     : boolean := false;
  --constant PACE_OSD_XPOS                    : natural := 0;
  --constant PACE_OSD_YPOS                    : natural := 0;

	-- Coco1-specific constants

  constant COCO1_USE_REAL_6809              : boolean := false;
  
  --constant COCO1_MC6847_ROM                 : string := "mc6847_pal.hex";
  constant COCO1_MC6847_ROM                 : string := "mc6847_ntsc.hex";
  --constant COCO1_MC6847_ROM                 : string := "mc6847t1_pal.hex";
  --constant COCO1_MC6847_ROM                 : string := "mc6847t1_ntsc.hex";
  
  --constant COCO1_BASIC_ROM                  : string := "bas10.hex";
  constant COCO1_BASIC_ROM                  : string := "bas11.hex";
  --constant COCO1_BASIC_ROM                  : string := "bas12.hex";
  --constant COCO1_EXTENDED_BASIC_ROM         : string := "extbas10.hex";
  constant COCO1_EXTENDED_BASIC_ROM         : string := "extbas11.hex";
  constant COCO1_EXTENDED_COLOR_BASIC       : boolean := true;
  
  constant ORCHESTRA90_ROM         : string := "Orchestra90.mif";
  
  constant COCO1_CART_INTERNAL              : boolean := true;
  constant COCO1_CART_WIDTHAD               : integer := 13;
 --constant COCO1_CART_NAME                  : string := "clowns.hex";     -- 8KB
  --constant COCO1_CART_NAME                  : string := "dod.hex";        -- 8KB
  --constant COCO1_CART_NAME                  : string := "galactic.hex";   -- 4KB
  --constant COCO1_CART_NAME                  : string := "megabug.hex";    -- 8KB
  --constant COCO1_CART_NAME                  : string := "nebula.hex";     -- 8KB
  --constant COCO1_CART_NAME                  : string := "hdbdoslba128MB_os9.hex";   -- 8KB
   
   constant COCO1_CART_NAME                  : string := "miniide.mif";   -- 8KB
	--constant COCO1_CART_NAME                  : string := "Orchestra90.mif";   -- 8KB

  constant COCO1_JUMPER_32K_RAM             : std_logic := '1';
	constant COCO1_CVBS                       : boolean := false;
  
  constant COCO1_HAS_IDE                    : boolean := true;
  
	-- derived - do not edit
  constant PACE_HAS_FLASH                   : boolean := not COCO1_CART_INTERNAL;
	constant COCO1_VGA                        : boolean := not COCO1_CVBS;
  
  type from_PROJECT_IO_t is record
    not_used  : std_logic;
  end record;

  type to_PROJECT_IO_t is record
    not_used  : std_logic;
  end record;

end;
