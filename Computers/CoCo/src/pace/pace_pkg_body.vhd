--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library work;

package body pace_pkg is

  function NULL_TO_FLASH return to_FLASH_t is
  begin
    return ((others => '0'), (others => '0'), '0', '0', '0');
  end NULL_TO_FLASH;
  
  function NULL_TO_SRAM return to_SRAM_t is
  begin
    return ((others => '0'), (others => '0'), (others => '0'), '0', '0', '0');
  end NULL_TO_SRAM;

  function NULL_TO_AUDIO return to_AUDIO_t is
  begin
    return ('0', (others => '0'), (others => '0'));
  end NULL_TO_AUDIO;

  function NULL_TO_SPI return to_SPI_t is
  begin
    return (others => '0');
  end NULL_TO_SPI;

  function NULL_TO_SERIAL return to_SERIAL_t is
  begin
    return (others => '0');
  end NULL_TO_SERIAL;

  function NULL_TO_SOUND return to_SOUND_t is
  begin
    return ((others => '0'), (others => '0'), '0', '0');
  end NULL_TO_SOUND;
  
  function NULL_FROM_OSD return from_OSD_t is
  begin
    return (others => (others => '0'));
  end NULL_FROM_OSD;

  function NULL_TO_OSD return to_OSD_t is
  begin
    return ('0', (others => '0'), (others => '0'), '0');
  end NULL_TO_OSD;

  function NULL_TO_GP return to_GP_t is
  begin
    return ((others => '0'), (others => '0'));
  end NULL_TO_GP;

end package body pace_pkg;
