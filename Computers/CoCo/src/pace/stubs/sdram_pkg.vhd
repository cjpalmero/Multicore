--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;

package sdram_pkg is

  component yadmc
    generic
    (
      sdram_depth         : natural := 25;
      sdram_columndepth   : natural := 9;
      sdram_adrwires      : natural := 13;
      sdram_bytes_depth   : natural := 1;
      cache_depth         : natural := 10;
      --sdram_bits          : natural := (8 << sdram_bytes_depth);
      sdram_bits          : natural;
      --cache_linedepth     : natural := sdram_bytes_depth + 1;
      cache_linedepth     : natural;
      --cache_linelength    : natural := (4 << cache_linedepth);
      cache_linelength    : natural;
      --cache_tagdepth      : natural := sdram_depth - cache_depth - cache_linedepth - 2
      cache_tagdepth      : natural
    );
    port
    (
      -- debug
      state         : out std_logic_vector(1 downto 0);
      statey        : out std_logic_vector(4 downto 0);
      
      -- WISHBONE interface
      sys_clk       : in std_logic;
      sys_rst       : in std_logic;
      wb_adr_i      : in std_logic_vector(31 downto 0);
      wb_dat_i      : in std_logic_vector(31 downto 0);
      wb_dat_o      : out std_logic_vector(31 downto 0);
      wb_sel_i      : in std_logic_vector(3 downto 0);
      wb_cyc_i      : in std_logic;
      wb_stb_i      : in std_logic;
      wb_we_i       : in std_logic;
      wb_ack_o      : out std_logic;

      -- SDRAM interface
      sdram_clk     : in std_logic;
      sdram_cke     : out std_logic;
      sdram_cs_n    : out std_logic;
      sdram_we_n    : out std_logic;
      sdram_cas_n   : out std_logic;
      sdram_ras_n   : out std_logic;
      sdram_dqm     : out std_logic_vector((sdram_bits/8)-1 downto 0);
      sdram_adr     : out std_logic_vector(sdram_adrwires-1 downto 0);
      sdram_ba      : out std_logic_vector(1 downto 0);
      sdram_dq      : inout std_logic_vector(sdram_bits-1 downto 0)
    );
  end component yadmc;
  
  -- Types

	type from_SDRAM_t is record
		d					: std_logic_vector(31 downto 0);
		ack       : std_logic;
	end record;
	
	type to_SDRAM_t is record
    clk       : std_logic;
    rst       : std_logic;
		a					: std_logic_vector(31 downto 0);
		d					: std_logic_vector(31 downto 0);
		sel			  : std_logic_vector(3 downto 0);
		cyc			  : std_logic;
		stb			  : std_logic;
		we				: std_logic;
	end record;

end;
