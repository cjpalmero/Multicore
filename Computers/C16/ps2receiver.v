/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//  Copyright 2013-2016 Istvan Hegedus
//
//  FPGATED is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  FPGATED is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
//
// Create Date:    11:30:06 12/14/2015 
// Module Name:    ps2receiver.v
// Project Name: 	 FPGATED
// Description: 	 PS2 keyboard receiver
//
// 
//
// Revision: 
// Revision 1.0 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ps2receiver(
    input clk,
    input ps2_clk,
    input ps2_data,
    output reg rx_done,
    output reg [7:0] ps2scancode
    );

reg ps2clkreg=1'b0,prev_ps2clkreg=1'b0;
reg [3:0] receivedbits=4'b0;
reg [11:0] watchdog=12'd2900;								// ~ 100us watchdog period with 28MHz clock
reg [7:0] ps2clkfilter;
reg [10:0] shiftreg;

always @(posedge clk)										// filtering ps2 clock line glitches
	begin
	ps2clkfilter<={ps2clkfilter[6:0],ps2_clk};
	if(ps2clkfilter==8'h00)
		ps2clkreg<=0;
	else if (ps2clkfilter==8'hff)
		ps2clkreg<=1;
	prev_ps2clkreg<=ps2clkreg;								// this is needed for clock edge detection
	end

always @(posedge clk)
	begin
	rx_done<=0;													// rx_done is active only for one clk cycle
	if(watchdog==0)											// when watchdog timer expires, reset received bits
		receivedbits<=0;
	else watchdog<=watchdog-1'd1;
		
	if(prev_ps2clkreg & ~ps2clkreg)						// falling edge of ps2 clock
		begin
		watchdog<=12'd2900;									// reload watchdog timer
		shiftreg<={ps2_data,shiftreg[10:1]};
		receivedbits<=receivedbits+1'd1;
		end
		
	if(receivedbits==4'd11)
		begin
		ps2scancode<=shiftreg[8:1];
		rx_done<=1;
		receivedbits<=0;
		end
	end

endmodule
