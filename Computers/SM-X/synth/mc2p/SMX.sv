/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//  MSX top level for MiST
// 
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================
//
//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module SMX_mc2p
(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on

);


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_sys ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

assign joy_p7_o = 1'b1;

reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, status[31], pump_s);

//-----------------------------------------------------------------

assign LED  = ~leds[0];

`include "build_id.v"
parameter CONF_STR = {
    "D,disable STM SD;",
    "OAB,Scanlines,Off,25%,50%,75%;",
    "OC,Blend,Off,On;",
    "O2,CPU Clock,Normal,Turbo;",
    "O3,Slot1,MegaSCC+ 1MB,Empty;",
  //  "O45,Slot2,Empty,MegaSCC+ 2MB,MegaRAM 1MB,MegaRAM 2MB;",    
    "O45,Slot2,MegaSCC+ 2MB,Empty,MegaRAM 2MB,MegaRAM 1MB;",    
    "O6,RAM,2048kB,4096kB;",
    "OD,Slot 0,Expanded,Primary;",
    "O7,OPL3 sound,Yes,No;",
//  "O8,VGA Output,CRT,LCD;",
    "O9,Tape sound,OFF,ON;",
    "O1,Scandoubler,On,Off;",
    "T0,Reset;",
    "V,v1.0.",`BUILD_DATE
};


////////////////////   CLOCKS   ///////////////////

wire pll_locked;
wire clk_sys;
wire memclk;

pll pll
(
    .inclk0(clock_50_i),
    .c0(clk_sys),
    .c1(memclk),
    .c2(SDRAM_CLK),
    .locked(pll_locked)
);

//assign SDRAM_CLK = memclk;

//////////////////   MiST I/O   ///////////////////
wire  [7:0] joy_0;
wire  [7:0] joy_1;
wire  [1:0] buttons;
wire [31:0] status;
wire        ypbpr;

reg  [31:0] sd_lba;
reg         sd_rd = 0;
reg         sd_wr = 0;
wire        sd_conf;
wire        sd_ack;
wire        sd_ack_conf;
wire        sd_sdhc;
wire  [8:0] sd_buff_addr;
wire  [7:0] sd_buff_dout;
wire  [7:0] sd_buff_din;
wire        sd_buff_wr;
wire        img_mounted;
wire        img_readonly;
wire [31:0] img_size;

wire        ps2_kbd_clk;
wire        ps2_kbd_data;

wire  [8:0] mouse_x;
wire  [8:0] mouse_y;
wire  [7:0] mouse_flags;
wire        mouse_strobe;

wire [63:0] rtc;
/*
user_io #(.STRLEN($size(CONF_STR)>>3), .PS2DIV(1500)) user_io
(
        .clk_sys(clk_sys),
        .clk_sd(clk_sys),
        .SPI_SS_IO(CONF_DATA0),
        .SPI_CLK(SPI_SCK),
        .SPI_MOSI(SPI_DI),
        .SPI_MISO(SPI_DO),

        .conf_str(CONF_STR),

        .status(status),
        .scandoubler_disable(scandoubler_disable),
        .ypbpr(ypbpr),
        .buttons(buttons),
        .rtc(rtc),

        .joystick_0(joy_0),
        .joystick_1(joy_1),

        .sd_conf(sd_conf),
        .sd_ack(sd_ack),
        .sd_ack_conf(sd_ack_conf),
        .sd_sdhc(sd_sdhc),
        .sd_rd(sd_rd),
        .sd_wr(sd_wr),
        .sd_lba(sd_lba),
        .sd_buff_addr(sd_buff_addr),
        .sd_din(sd_buff_din),
        .sd_dout(sd_buff_dout),
        .sd_dout_strobe(sd_buff_wr),

        .ps2_kbd_clk(ps2_kbd_clk),
        .ps2_kbd_data(ps2_kbd_data),

        .mouse_x(mouse_x),
        .mouse_y(mouse_y),
        .mouse_flags(mouse_flags),
        .mouse_strobe(mouse_strobe),

        // unused
        .switches(),
        .ps2_mouse_clk(),
        .ps2_mouse_data(),
        .joystick_analog_0(),
        .joystick_analog_1()
);

sd_card sd_card
(
        .clk_sys(clk_sys),
        .img_mounted(img_mounted),
        .img_size(img_size),
        .sd_conf(sd_conf),
        .sd_ack(sd_ack),
        .sd_ack_conf(sd_ack_conf),
        .sd_sdhc(sd_sdhc),
        .sd_rd(sd_rd),
        .sd_wr(sd_wr),
        .sd_lba(sd_lba),
        .sd_buff_addr(sd_buff_addr),
        .sd_buff_din(sd_buff_din),
        .sd_buff_dout(sd_buff_dout),
        .sd_buff_wr(sd_buff_wr),
        .allow_sdhc(1),
        .sd_sck(Sd_Ck),
        .sd_cs(Sd_Dt[3]),
        .sd_sdi(Sd_Cm),
        .sd_sdo(Sd_Dt[0])
);
*/

reg [7:0]osd_s;

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io
(
    .clk_sys    ( clk_sys ),
    .SPI_SCK    ( SPI_SCK ),
    .SPI_DI     ( SPI_DI  ),
    .SPI_SS2    ( SPI_SS2 ),
    .SPI_DO     ( SPI_DO  ),
    
    .data_in    ( osd_s & pump_s  ),
    .conf_str   ( CONF_STR ),
    .status     ( status  )
);

wire [5:0] audio_li;
wire [5:0] audio_ri;
wire [5:0] audio_l;
wire [5:0] audio_r;

wire [5:0] joya = joy_0[5:0];
wire [5:0] joyb = joy_1[5:0];
wire [5:0] msx_joya;
wire [5:0] msx_joyb;
wire       msx_stra;
wire       msx_strb;

wire       Sd_Ck;
wire       Sd_Cm;
wire [3:0] Sd_Dt;

wire       msx_ps2_kbd_clk = ps2_kbd_clk;
wire       msx_ps2_kbd_data = (ps2_kbd_data == 1'b0 ? ps2_kbd_data : 1'bZ);
reg  [7:0] dipsw;
wire [7:0] leds;

reg reset;
wire resetW = status[0] | ~btn_n_i[4];

always @(posedge clk_sys) begin
    reset <= resetW;
    dipsw <= {1'b0, ~status[6], ~status[5], status[4], status[3], ~status[1] & status[8], status[1], ~status[2]};
    audio_li <= audio_l;
    if (status[9]) begin        
        audio_ri <= audio_r;
    end 
        else audio_ri<= audio_l;
end

assign joy_0 = {joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i};
assign joy_1 = {joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i};


assign msx_joya = mouse_en ? (mouse ? 1'bZ : mouse) : (~joya & ~msx_stra ? joya : 1'bZ);
assign msx_joyb =                                     (~joyb & ~msx_strb ? joyb : 1'bZ);

reg        mouse_en = 0;
reg  [5:0] mouse;

/*
always_comb begin
    for (integer i=0; i<=5; i++) begin
        msx_joya[i] <= mouse_en ? (mouse[i] ? 1'bZ : mouse[i]) : (~joya[i] & ~msx_stra ? joya[i] : 1'bZ);
        msx_joyb[i] <=                                           (~joyb[i] & ~msx_strb ? joyb[i] : 1'bZ);
    end
end

always @(posedge clk_sys) begin

    reg        stra_d;
    reg  [8:0] mouse_x_latch;
    reg  [8:0] mouse_y_latch;
    reg  [1:0] mouse_state;
    reg [17:0] mouse_timeout;

    if (reset) begin
        mouse_en <= 0;
        mouse_state <= 0;
    end
    else if (mouse_strobe) mouse_en <= 1;
    else if (~&joya) mouse_en <= 0;

    if (mouse_strobe) begin
        mouse_x_latch <= ~mouse_x + 1'd1; //2nd complement of x
        mouse_y_latch <= mouse_y;
    end

    mouse[5:4] <= ~mouse_flags[1:0];
    if (mouse_en) begin
        if (mouse_timeout) begin
            mouse_timeout <= mouse_timeout - 1'd1;
            if (mouse_timeout == 1) mouse_state <= 0;
        end

        stra_d <= msx_stra;
        if (stra_d ^ msx_stra) begin
            mouse_timeout <= 18'd100000;
            mouse_state <= mouse_state + 1'd1;
            case (mouse_state)
            2'b00: mouse[3:0] <= {mouse_x_latch[5],mouse_x_latch[6],mouse_x_latch[7],mouse_x_latch[8]};
            2'b01: mouse[3:0] <= {mouse_x_latch[1],mouse_x_latch[2],mouse_x_latch[3],mouse_x_latch[4]};
            2'b10: mouse[3:0] <= {mouse_y_latch[5],mouse_y_latch[6],mouse_y_latch[7],mouse_y_latch[8]};
            2'b11:
            begin
                mouse[3:0] <= {mouse_y_latch[1],mouse_y_latch[2],mouse_y_latch[3],mouse_y_latch[4]};
                mouse_x_latch <= 0;
                mouse_y_latch <= 0;
            end
            endcase
        end
    end
end
*/

//assign sd_sclk_o = Sd_Ck;
//assign sd_mosi_o = Sd_Cm;
//assign sd_cs_n_o = Sd_Dt[3];
//assign Sd_Dt[0] = sd_miso_i;

emsx_top emsx
(
//        -- Clock, Reset ports
        .clk21m     (clk_sys),
        .memclk     (memclk),
        .pSltRst_n  (~reset),

//        -- SD-RAM ports
        .pMemAdr   ( SDRAM_A ),
        .pMemDat   ( SDRAM_DQ ),
        .pMemLdq   ( SDRAM_DQML ),
        .pMemUdq   ( SDRAM_DQMH ),
        .pMemWe_n  ( SDRAM_nWE ),
        .pMemCas_n ( SDRAM_nCAS ),
        .pMemRas_n ( SDRAM_nRAS ),
        .pMemCs_n  ( SDRAM_nCS ),
        .pMemBa0   ( SDRAM_BA[0] ),
        .pMemBa1   ( SDRAM_BA[1] ),
        .pMemCke   ( SDRAM_CKE ),

//        -- PS/2 keyboard ports
        .pPs2Clk   (ps2_clk_io),
        .pPs2Dat   (ps2_data_io),

//        -- Joystick ports (Port_A, Port_B)
        .pJoyA      ( {msx_joya[5:4], msx_joya[0], msx_joya[1], msx_joya[2], msx_joya[3]} ),
        .pStra      ( msx_stra ),
        .pJoyB      ( {msx_joyb[5:4], msx_joyb[0], msx_joyb[1], msx_joyb[2], msx_joyb[3]} ),
        .pStrb      ( msx_strb ),

//        -- SD/MMC slot ports
        .pSd_Ck     (sd_sclk_o), // (Sd_Ck),
        .pSd_Cm     (sd_mosi_o), // (Sd_Cm),
        .pSd_Dt     ({sd_cs_n_o,2'bZZ,sd_miso_i}), // (Sd_Dt),

//        -- DIP switch, Lamp ports
        .pDip       (dipsw),
        .pLed       (leds),

//        -- Video, Audio/CMT ports
        .pDac_VR    (R_O),      // RGB_Red / Svideo_C
        .pDac_VG    (G_O),      // RGB_Grn / Svideo_Y
        .pDac_VB    (B_O),      // RGB_Blu / CompositeVideo
        .pVideoHS_n (HSync),    // HSync(RGB15K, VGA31K)
        .pVideoVS_n (VSync),    // VSync(RGB15K, VGA31K)

        .CmtIn      (ear_i),
        .pDac_SL    (audio_l),
        .pDac_SR    (audio_r),
        
        .osd_o      (osd_s),
        .opl3_enabled ( ~status[7] ),
        .slot0_exp  ( ~status[13] )
);

//////////////////   VIDEO   //////////////////
wire  [5:0] R_O;
wire  [5:0] G_O;
wire  [5:0] B_O;
wire        HSync, VSync, CSync;

wire [5:0] osd_r_o, osd_g_o, osd_b_o;
/*
osd osd
(
    .clk_sys(clk_sys),
    .SPI_DI(SPI_DI),
    .SPI_SCK(SPI_SCK),
    .SPI_SS3(SPI_SS2),
    .R_in(R_O),
    .G_in(G_O),
    .B_in(B_O),
    .HSync(HSync),
    .VSync(VSync),
    .R_out(osd_r_o),
    .G_out(osd_g_o),
    .B_out(osd_b_o)
    );

assign VGA_R  = osd_r_o;
assign VGA_G  = osd_g_o;
assign VGA_B  = osd_b_o;
assign VGA_HS = HSync;
assign VGA_VS = VSync;
*/
     mist_video #( .OSD_COLOR ( 3'b001 )) mist_video_inst 
     (
         .clk_sys     ( memclk ),
         .scanlines   ( status[11:10] ),
         .rotate      ( 2'b00 ),
         .scandoubler_disable  ( 1'b1 ),
         .ce_divider  ( 1'b0 ), //1 imagem ok clk21 ou 0 com clksdram para usar blend
         .blend       ( status[12] ),
 
         .SPI_SCK     ( SPI_SCK ),
         .SPI_SS3     ( SPI_SS2 ),
         .SPI_DI      ( SPI_DI ),
 
         .HSync       ( HSync ), 
         .VSync       ( VSync ), 
         .R           ( R_O ),
         .G           ( G_O ),
         .B           ( B_O ),
 
         .VGA_HS      ( VGA_HS ),
         .VGA_VS      ( VGA_VS ),
         .VGA_R       ( VGA_R ),
         .VGA_G       ( VGA_G ),
         .VGA_B       ( VGA_B ),
 
         .osd_enable  ( )
     );

assign AUDIO_L = audio_li[0];
assign AUDIO_R = audio_ri[0];
/*
//////////////////   AUDIO   //////////////////
dac #(6) dac_l
(
    .clk_i(clk_sys),
    .res_n_i(1'b1),
    .dac_i(audio_li),
    .dac_o(AUDIO_L)
);

dac #(6) dac_r
(
    .clk_i(clk_sys),
    .res_n_i(1'b1),
    .dac_i(audio_ri),
    .dac_o(AUDIO_R)
);
*/
endmodule
