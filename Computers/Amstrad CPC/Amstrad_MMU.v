/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*

	Reworked Amstrad MMU for simplicity
	(C) 2018 Sorgelig

--------------------------------------------------------------------------------
--    {@{@{@{@{@{@
--  {@{@{@{@{@{@{@{@  This code is covered by CoreAmstrad synthesis r004
--  {@    {@{@    {@  A core of Amstrad CPC 6128 running on MiST-board platform
--  {@{@{@{@{@{@{@{@
--  {@  {@{@{@{@  {@  CoreAmstrad is implementation of FPGAmstrad on MiST-board
--  {@{@        {@{@   Contact : renaudhelias@gmail.com
--  {@{@{@{@{@{@{@{@   @see http://code.google.com/p/mist-board/
--    {@{@{@{@{@{@     @see FPGAmstrad at CPCWiki
--
--
--------------------------------------------------------------------------------
-- FPGAmstrad_amstrad_motherboard.Amstrad_MMU
-- RAM ROM mapping split
--------------------------------------------------------------------------------
*/

//http://www.grimware.org/doku.php/documentations/devices/gatearray

module Amstrad_MMU
(
	input        CLK,
	input        reset,

	input        ram64k,

	input        mem_WR,
	input        io_WR,

	input  [7:0] D,
	input [15:0] A,
	output reg [22:0] ram_A
);

reg lowerROMen;
reg upperROMen;
reg [2:0] RAMmap;
reg [2:0] RAMpage;
reg [7:0] ROMbank;

always @(posedge CLK) begin
	reg old_wr = 0;

	if (reset) begin
		ROMbank    <=0;
		RAMmap     <=0;
		RAMpage    <=0;
		lowerROMen <=1;
		upperROMen <=1;
	end
	else begin
		old_wr <= io_WR;
		if (~old_wr & io_WR) begin
			if (A[15:14] == 'b01 && D[7:6] == 'b10) begin //7Fxx gate array RMR
				lowerROMen <= ~D[2];
				upperROMen <= ~D[3];
			end
			
			if (~A[15] && D[7:6] == 'b11 && ~ram64k) begin //7Fxx PAL MMR
				RAMpage <= D[5:3];
				RAMmap  <= D[2:0];
			end

			if (~A[13]) ROMbank <= D;
		end
	end
end

always @(*) begin
	casex({lowerROMen&~mem_WR&(!A[15:14]), upperROMen&~mem_WR&(&A[15:14]), RAMmap, A[15:14]})
		'b1x_xxx_xx: ram_A[22:14] = 0;                // lower rom
		'b01_xxx_xx: ram_A[22:14] = {1'b1,  ROMbank}; // upper rom
		'b00_0x1_11,                                                      // map1&3 bank3
		'b00_010_xx: ram_A[22:14] = {2'b00, RAMpage, 2'b11,    A[15:14]}; // map2 bank0-3
		'b00_011_01: ram_A[22:14] = {2'b00,  3'b000, 2'b10,       2'b11}; // map3 bank1
		'b00_1xx_01: ram_A[22:14] = {2'b00, RAMpage, 2'b11, RAMmap[1:0]}; // map4 bank1
		    default: ram_A[22:14] = {2'b00,  3'b000, 2'b10,    A[15:14]}; // default 64KB map
	endcase

	ram_A[13:0] = A[13:0];
end

endmodule
