--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- -----------------------------------------------------------------------
--
--                                 FPGA 64
--
--     A fully functional commodore 64 implementation in a single FPGA
--
-- -----------------------------------------------------------------------
-- Copyright 2005-2008 by Peter Wendrich (pwsoft@syntiac.com)
-- http://www.syntiac.com/fpga64.html
-- -----------------------------------------------------------------------
--
-- Reset circuit
--
-- -----------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.ALL;
use IEEE.numeric_std.all;

entity fpga64_busTiming is
	generic (
		resetCycles: integer := 15;
		noofBusCycles : integer := 52
	);
	port (
		clkIn : in std_logic;
		rstIn : in std_logic;		
		rstOut : out std_logic;
		endOfCycle : out std_logic; -- Signal is 1 on last count of current cycle.
		busCycle : out unsigned(5 downto 0)
	);
end fpga64_busTiming;

-- -----------------------------------------------------------------------

architecture rtl of fpga64_busTiming is
signal clk33 : std_logic;
signal nextCycle : std_logic;
signal resetCycleCounter : integer range 0 to resetCycles := 0;
signal busCycleCounter : unsigned(5 downto 0) := (others => '0');
begin
	clk33 <= clkIn;

	process(clk33)
	begin
		if rising_edge(clk33) then
			if (busCycleCounter = (noofBusCycles - 2) ) then
				nextCycle <= '1';
			else
				nextCycle <= '0';
			end if;
			if nextCycle = '1' then
				busCycleCounter <= (others => '0');
			else
				busCycleCounter <= busCycleCounter + 1;
			end if;
			if resetCycleCounter = resetCycles then
				rstOut <= '0';
			else
				rstOut <= '1';
				if nextCycle = '1' then
					resetCycleCounter <= resetCycleCounter + 1;
				end if;
			end if;
			if rstIn = '1' then
--				nextCycle <= '0';
				resetCycleCounter <= 0;
			end if;
		end if;
	end process;
	busCycle <= busCycleCounter;
	endOfCycle <= nextCycle;
end architecture;
