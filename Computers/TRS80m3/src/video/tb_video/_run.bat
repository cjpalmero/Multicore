vlib work

vcom ..\..\rom\tile_rom.vhd
IF ERRORLEVEL 1 GOTO error

vcom ..\trs_video.vhd
IF ERRORLEVEL 1 GOTO error

vcom tb_trs_video.vht
IF ERRORLEVEL 1 GOTO error

vsim -t ns tb -do all.do
IF ERRORLEVEL 1 GOTO error

goto ok

:error
echo Error!
pause

:ok
