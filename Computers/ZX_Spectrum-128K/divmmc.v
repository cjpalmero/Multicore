/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// divmmc

//
// Total refactoring. Made module synchroneous. (Sorgelig)
//

module divmmc
(
    input         clk_sys,
    input   [1:0] mode,

	// CPU interface
    input         nWR,
    input         nRD,
    input         nMREQ,
    input         nIORQ,
    input         nM1,
    input  [15:0] addr,
    input   [7:0] din,
    output  [7:0] dout,

    // control
    input         enable,
    output        active_io,

	// SD/MMC SPI
    output reg    spi_ss,
    output        spi_clk,
    input         spi_di,
    output        spi_do
);

assign    active_io   = port_io;

wire      io_we = ~nIORQ & ~nWR & nM1;
wire      io_rd = ~nIORQ & ~nRD & nM1;

wire      port_cs = ((mode == 2'b01) && (addr[7:0] == 8'hE7)) ||
                    ((mode == 2'b10) && (addr[7:0] == 8'h1F));
wire      port_io = ((mode == 2'b01) && (addr[7:0] == 8'hEB)) ||
                    ((mode == 2'b10) && (addr[7:0] == 8'h3F));

reg       tx_strobe;
reg       rx_strobe;

always @(posedge clk_sys) begin
	reg old_we, old_rd, old_m1;
	reg m1_trigger;

	rx_strobe <= 0;
	tx_strobe <= 0;

	if(enable) begin

		old_we <= io_we;
		old_rd <= io_rd;

		if(io_we & ~old_we) begin
            if (port_cs) spi_ss <= din[0];                          // SPI enable
            if (port_io) tx_strobe <= 1'b1;                         // SPI write
		end

		// SPI read
		if(io_rd & ~old_rd & port_io) rx_strobe <= 1;

	end else begin
		spi_ss     <= 1;
	end
end

spi spi
(
   .clk_sys(clk_sys),
   .tx(tx_strobe),
   .rx(rx_strobe),
   .din(din),
   .dout(dout),

   .spi_clk(spi_clk),
   .spi_di(spi_di),
   .spi_do(spi_do)
);

endmodule

module spi
(
	input        clk_sys,

	input        tx,        // Byte ready to be transmitted
	input        rx,        // request to read one byte
	input  [7:0] din,
	output [7:0] dout,

	output       spi_clk,
	input        spi_di,
	output       spi_do
);

assign    spi_clk = counter[0];
assign    spi_do  = io_byte[7]; // data is shifted up during transfer
assign    dout    = data;

reg [4:0] counter = 5'b10000;  // tx/rx counter is idle
reg [7:0] io_byte, data;

always @(negedge clk_sys) begin
    if(counter[4]) begin
        if(rx | tx) begin
            counter <= 0;
            data <= io_byte;
            io_byte <= tx ? din : 8'hff;
        end
    end else begin
        if(spi_clk) io_byte <= { io_byte[6:0], spi_di };
        counter <= counter + 2'd1;
    end
end

endmodule
