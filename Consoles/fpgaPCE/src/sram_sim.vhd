--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_TEXTIO.all;
library STD;
use STD.TEXTIO.ALL;

entity sram_sim is
	generic(
		INIT_FILE	:	string := "vram.txt"
	);
	port(
		A 	: in std_logic_vector(17 downto 0);

		OEn	: in std_logic;
		WEn	: in std_logic;			
		CEn	: in std_logic;

		UBn	: in std_logic;
		LBn	: in std_logic;
		
		DQ	: inout std_logic_vector(15 downto 0)
	);
end sram_sim;

architecture sim of sram_sim is
begin

	-- process
		-- file F		: text open read_mode is INIT_FILE;
		-- variable L	: line;
		-- variable V	: std_logic_vector(15 downto 0);
	-- begin
        -- for i in 0 to 32767 loop     
          -- exit when endfile(F); 
          -- readline(F,L);  
          -- hread(L,V);
          -- RAMEXT(i) <= V;
        -- end loop;

		-- wait;
	-- end process;
	
	process(CEn, OEn, WEn, UBn, LBn, A, DQ)
		file F		: text open read_mode is INIT_FILE;
		variable L	: line;
		variable V	: std_logic_vector(15 downto 0);
		variable init_done : std_logic := '0';
		type memory is array(natural range <>) of std_logic_vector(15 downto 0);
		variable RAMEXT : memory(0 to 2**18 - 1) := (others => x"ffff");
	begin
		if init_done = '0' then
			for i in 0 to 32767 loop     
				exit when endfile(F); 
				readline(F,L);  
				hread(L,V);
				RAMEXT(i) := V;
			end loop;
			init_done := '1';
		end if;

		if CEn = '0' then

			if OEn = '0' and LBn = '0' then
				DQ(7 downto 0) <= RAMEXT(conv_integer(to_x01(A)))(7 downto 0);
			else
				DQ(7 downto 0) <= "ZZZZZZZZ";
			end if;
			if OEn = '0' and UBn = '0' then
				DQ(15 downto 8) <= RAMEXT(conv_integer(to_x01(A)))(15 downto 8);
			else
				DQ(15 downto 8) <= "ZZZZZZZZ";
			end if;

			if WEn = '0' and LBn = '0' then
				RAMEXT(conv_integer(to_x01(A)))(7 downto 0) := DQ(7 downto 0);
			end if;
			if WEn = '0' and UBn = '0' then
				RAMEXT(conv_integer(to_x01(A)))(15 downto 8) := DQ(15 downto 8);
			end if;
			
		else

			DQ(15 downto 0) <= "ZZZZZZZZZZZZZZZZ";

		end if;
	end process;
		
end sim;

