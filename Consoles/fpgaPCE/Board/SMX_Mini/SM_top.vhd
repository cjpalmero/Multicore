--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
	port (
			-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(2 downto 1);
		dip_i					: in    std_logic_vector(8 downto 1);
		
		-- SDRAM	(H57V256)
		sdram_ad_o			: out std_logic_vector(12 downto 0);
		sdram_da_io			: inout std_logic_vector(15 downto 0);

		sdram_ba_o			: out std_logic_vector(1 downto 0);
		sdram_dqm_o			: out std_logic_vector(1 downto 0);

		sdram_ras_o			: out std_logic;
		sdram_cas_o			: out std_logic;
		sdram_cke_o			: out std_logic;
		sdram_clk_o			: out std_logic;
		sdram_cs_o			: out std_logic;
		sdram_we_o			: out std_logic;
	

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: inout std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: inout std_logic;
		sd_sw_i				: in    std_logic;

		-- Joysticks
		joy1_up_io			: inout std_logic;
		joy1_down_io		: inout std_logic;
		joy1_left_io		: inout std_logic;
		joy1_right_io		: inout std_logic;
		joy1_p6_io			: inout std_logic;
		joy1_p7_io			: inout std_logic;
		joy1_p8_io			: inout std_logic;
		
		joy2_up_io			: inout std_logic;
		joy2_down_io		: inout std_logic;
		joy2_left_io		: inout std_logic;
		joy2_right_io		: inout std_logic;
		joy2_p6_io			: inout std_logic;
		joy2_p7_io			: inout std_logic;
		joy2_p8_io			: inout std_logic;
	
	
		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(4 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- External Slots
		slot_A_o				: inout   std_logic_vector(15 downto 0)	:= (others => 'Z');
		slot_D_io			: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		slot_CS1_o			: inout   std_logic 							:= 'Z';
		slot_CS2_o			: inout   std_logic 							:= 'Z';
		slot_CLOCK_o		: inout   std_logic 							:= 'Z';		
		slot_M1_o			: inout   std_logic 							:= 'Z';		
		slot_MREQ_o			: inout   std_logic 							:= 'Z';		
		slot_IOREQ_o		: inout   std_logic 							:= 'Z';		
		slot_RD_o			: inout   std_logic 							:= 'Z';		
		slot_WR_o			: inout   std_logic 							:= 'Z';		
		slot_RESET_io		: inout std_logic 							:= 'Z';		
		slot_SLOT1_o		: inout   std_logic 							:= 'Z';		
		slot_SLOT2_o		: inout   std_logic 							:= 'Z';			
		slot_SLOT3_o		: inout   std_logic 							:= 'Z';	
		slot_BUSDIR_i		: inout    std_logic 						:= 'Z';	
		slot_RFSH_i			: inout    std_logic 						:= 'Z';	
		slot_INT_i			: inout    std_logic 						:= 'Z';	
		slot_WAIT_i			: inout    std_logic 						:= 'Z';	
		
		slot_DATA_OE_o		: out std_logic 								:= 'Z';	
		slot_DATA_DIR_o	: out std_logic 								:= 'Z';	

		-- HDMI video
		hdmi_pclk			: out std_logic		:= 'Z';  
		hdmi_de				: out std_logic		:= 'Z';  
		hdmi_int				: in  std_logic		:= 'Z';  
		hdmi_rst				: out std_logic		:= '1';  
		
		-- HDMI audio
		aud_sck				: out std_logic		:= 'Z';  
		aud_ws				: out std_logic		:= 'Z';  
		aud_i2s				: out std_logic		:= 'Z';  
		
		-- HDMI programming
		hdmi_sda				: inout std_logic		:= 'Z';  
		hdmi_scl				: inout std_logic		:= 'Z';  
		
		--ESP
		esp_rx_o				: out std_logic		:= 'Z'; 
		esp_tx_i				: in  std_logic		:= 'Z'
	);
end entity;

architecture Behavior of top is

	-- Sigma Delta audio
	COMPONENT hybrid_pwm_sd
		PORT
		(
			clk		:	 IN STD_LOGIC;
			n_reset		:	 IN STD_LOGIC;
			din		:	 IN STD_LOGIC_VECTOR(15 DOWNTO 0);
			dout		:	 OUT STD_LOGIC
		);
	END COMPONENT;


	----------------------------------------
	-- internal signals                   --
	----------------------------------------

	-- clock
	signal pll_in_clk : std_logic;
	signal clk_114 	: std_logic;
	signal clk_28 		: std_logic;
	signal pll_locked : std_logic;
	signal clk_7 		: std_logic;
	signal c1 			: std_logic;
	signal c3 			: std_logic;
	signal cck 			: std_logic;
	signal eclk 		: std_logic_vector (10-1 downto 0);
	signal clk_50 		: std_logic;
	signal sysclk 		: std_logic;
	signal memclk 		: std_logic;
	signal clk_vga 		: std_logic;
 
	-- reset
	signal pll_rst 	: std_logic;
	signal sdctl_rst 	: std_logic;
	signal rst_50 		: std_logic;
	signal rst_minimig: std_logic;
 
	-- ctrl 
	signal rom_status : std_logic;
	signal ram_status : std_logic;
	signal reg_status : std_logic;
 
	-- tg68
	signal tg68_rst	 	: std_logic;
	signal tg68_dat_in 	: std_logic_vector (16-1 downto 0);
	signal tg68_dat_out 	: std_logic_vector (16-1 downto 0);
	signal tg68_adr 		: std_logic_vector (32-1 downto 0);
	signal tg68_IPL 		: std_logic_vector (3-1 downto 0);
	signal tg68_dtack 	: std_logic;
	signal tg68_as 		: std_logic;
	signal tg68_uds 		: std_logic;
	signal tg68_lds 		: std_logic;
	signal tg68_rw 		: std_logic;
	signal tg68_ena7RD 	: std_logic;
	signal tg68_ena7WR 	: std_logic;
	signal tg68_enaWR 	: std_logic;
	signal tg68_cout 		: std_logic_vector (16-1 downto 0);
	signal tg68_cpuena 	: std_logic;
	signal cpu_config 	: std_logic_vector (2-1 downto 0);
	signal memcfg 			: std_logic_vector (6-1 downto 0);
	signal tg68_cad 		: std_logic_vector (32-1 downto 0);
	signal tg68_cpustate : std_logic_vector (6-1 downto 0);
	signal tg68_cdma 		: std_logic;
	signal tg68_clds 		: std_logic;
	signal tg68_cuds 		: std_logic;
 
	-- minimig
	signal ram_data 		: std_logic_vector (16-1 downto 0);      -- sram data bus
	signal ramdata_in 	: std_logic_vector (16-1 downto 0);    -- sram data bus in
	signal ram_address 	: std_logic_vector (22-1 downto 0);   -- sram address bus
	signal ram_bhe 		: std_logic;      							-- sram upper byte select
	signal ram_ble 		: std_logic;      							-- sram lower byte select
	signal ram_we 			: std_logic;      							 -- sram write enable
	signal ram_oe 			: std_logic;      						 -- sram output enable
	signal s15khz 			: std_logic;      						  -- scandoubler disable
	signal joy_emu_en 	: std_logic;   							 -- joystick emulation enable
	signal sdo 				: std_logic;         					  -- SPI data output
	signal ldata 			: std_logic_vector (15-1 downto 0);   -- left DAC data
	signal rdata 			: std_logic_vector (15-1 downto 0);         -- right DAC data
	signal audio_left 	: std_logic_vector (15 downto 0);
	signal audio_right 	: std_logic_vector (15 downto 0);
	signal floppy_fwr 	: std_logic;
	signal floppy_frd 	: std_logic;
	signal hd_fwr 			: std_logic;
	signal hd_frd 			: std_logic;
 
	-- sdram
	signal reset_out	: std_logic;
	signal sdram_cs 	: std_logic_vector (4-1 downto 0);
	signal sdram_dqm 	: std_logic_vector (2-1 downto 0);
	signal sdram_ba 	: std_logic_vector (2-1 downto 0);
 
	-- audio
	signal audio_lr_switch	: std_logic;
	signal audio_lr_mix		: std_logic;
	
	signal audio_l 			: signed(15 downto 0);
	signal audio_r 			: signed(15 downto 0);
 
	-- ctrl
	signal SRAM_DAT_W		: std_logic_vector (16-1 downto 0);
	signal SRAM_DAT_R		: std_logic_vector (16-1 downto 0);
	signal FL_DAT_W		: std_logic_vector (8-1 downto 0);
	signal FL_DAT_R		: std_logic_vector (8-1 downto 0);
	signal SPI_CS_N		: std_logic_vector (4-1 downto 0);
	signal SPI_DI			: std_logic;
	signal rst_ext			: std_logic;
	signal boot_sel		: std_logic;
	signal ctrl_cfg		: std_logic_vector (4-1 downto 0);
	signal ctrl_status	: std_logic_vector (4-1 downto 0);

	-- indicators
	signal track	: std_logic_vector (8-1 downto 0);
 
	--audio
	signal audio_LR	: std_logic;
 
	signal vga_red		: unsigned (7 downto 0);
	signal vga_green	: unsigned (7 downto 0);
	signal vga_blue	: unsigned (7 downto 0);	
	signal vga_hsync_n_s : std_logic;
	signal vga_vsync_n_s : std_logic;
	signal vga_blank_s : std_logic;
		
	signal lpf1_wave_L	: std_logic_vector (15 downto 0);
	signal lpf5_wave_L	: std_logic_vector (15 downto 0);
	signal lpf1_wave_R	: std_logic_vector (15 downto 0);
	signal lpf5_wave_R	: std_logic_vector (15 downto 0);

	-- PS/2 keyboard
	signal PS2K_DAT_IN		: std_logic;
	signal PS2K_CLK_IN		: std_logic;
	signal PS2K_DAT_OUT		: std_logic;
	signal PS2K_CLK_OUT		: std_logic;
 
	-- PS/2 Mouse
	signal PS2M_DAT_IN		: std_logic;
	signal PS2M_CLK_IN		: std_logic;
	signal PS2M_DAT_OUT		: std_logic;
	signal PS2M_CLK_OUT		: std_logic;
	
	-- joystick
	signal joy1_s			: std_logic_vector(11 downto 0) := (others => '1'); -- MS ZYX CBA RLDU	
	signal joy2_s			: std_logic_vector(11 downto 0) := (others => '1'); -- MS ZYX CBA RLDU	
	signal joyP7_s			: std_logic;
	



begin

PS2K_DAT_IN <= ps2_data_io;
PS2K_CLK_IN <= ps2_clk_io;
PS2M_DAT_IN <= ps2_mouse_data_io;
PS2M_CLK_IN <= ps2_mouse_clk_io;

ps2_data_io 		<= '0' when (PS2K_DAT_OUT = '0') else 'Z'; 
ps2_clk_io 			<= '0' when (PS2K_CLK_OUT = '0') else 'Z'; 
ps2_mouse_data_io <= '0' when (PS2M_DAT_OUT = '0') else 'Z'; 
ps2_mouse_clk_io 	<= '0' when (PS2M_CLK_OUT = '0') else 'Z'; 

--vga_r_o <= vga_red(7 downto 4);
--vga_g_o <= vga_green(7 downto 4);
--vga_b_o <= vga_blue(7 downto 4);
vga_vsync_n_o <= vga_vsync_n_s;
vga_hsync_n_o <= vga_hsync_n_s;

slot_RESET_io <= '1';


	U00: work.pll 
	port map 
	(
		inclk0		=> clock_50_i,		-- external Clock 50 MHz 
		c0				=> sysclk,			-- 41.667
		c1				=> memclk,			-- 125 mhz
		c2				=> sdram_clk_o,	-- 125 mhz shifted
		c3				=> clk_vga			-- 25 mhz
	);
	


--	u_interpo_L: work.INTERPO 
--	generic map
--	(
--		msbi => 15
--	)
--	port map 
--	(
--		clk21m  => sysclk,
--		reset   => '0',
--		clkena  => '1',
--		idata   => "00" & audio_left(15 downto 2),
--		odata   => lpf1_wave_L
--	);
--	
--	u_lpf2_L: work.lpf2 
--	generic map
--	(
--		msbi => 15
--	)
--	port map 
--	(
--		clk21m  => sysclk,
--		reset   => '0',
--		clkena  => '1',
--		idata   => lpf1_wave_L,
--		odata   => lpf5_wave_L
--	);
--
--	u_interpo_R: work.INTERPO 
--	generic map
--	(
--		msbi => 15
--	)
--	port map 
--	(
--		clk21m  => sysclk,
--		reset   => '0',
--		clkena  => '1',
--		idata   => "00" & audio_right(15 downto 2),
--		odata   => lpf1_wave_R
--	);
--	
--	u_lpf2_R: work.lpf2 
--	generic map
--	(
--		msbi => 15
--	)
--	port map 
--	(
--		clk21m  => sysclk,
--		reset   => '0',
--		clkena  => '1',
--		idata   => lpf1_wave_R,
--		odata   => lpf5_wave_R
--	);
--
--
--	dacL: work.dac 
--	generic map
--	(
--		msbi_g => 15
--	)
--	port map 
--	(
--		clk_i  	=> sysclk,
--		res_n_i  => '1',
--		dac_i   	=> lpf5_wave_L,
--		dac_o   	=> dac_l_o
--	);
--	 
--	dacR: work.dac 
--	generic map
--	(
--		msbi_g => 15
--	)
--	port map 
--	(
--		clk_i  	=> sysclk,
--		res_n_i  => '1',
--		dac_i   	=> lpf5_wave_R,
--		dac_o   	=> dac_r_o
--	);


	myvt: work.Virtual_Toplevel 
	generic map
	(
		rowAddrBits => 13,
		colAddrBits => 9
	)
	port map 
	(
		reset => btn_n_i(1),
		CLK => sysclk,
		SDR_CLK => memclk,
		
		--SW => SW,
		
		DRAM_ADDR => sdram_ad_o,
		DRAM_DQ => sdram_da_io,
		DRAM_BA_1 => sdram_ba_o(1),
		DRAM_BA_0 => sdram_ba_o(0),
		DRAM_CKE => sdram_cke_o,
		DRAM_UDQM => sdram_dqm_o(1),
		DRAM_LDQM => sdram_dqm_o(0),
		DRAM_CS_N => sdram_cs_o,
		DRAM_WE_N => sdram_we_o,
		DRAM_CAS_N => sdram_cas_o,
		DRAM_RAS_N => sdram_ras_o,
		
--		DAC_LDATA => audio_left,
--		DAC_RDATA => audio_right,
		signed(DAC_LDATA) => audio_l,
		signed(DAC_RDATA) => audio_r,
		
		unsigned(VGA_R) => vga_red,
		unsigned(VGA_G) => vga_green,
		unsigned(VGA_B) => vga_blue,
		VGA_VS => vga_vsync_n_s,
		VGA_HS => vga_hsync_n_s,
		VGA_BLANK => vga_blank_s,
		
		RS232_RXD => '1',
		RS232_TXD => open,
		
		-- PS/2
		ps2k_clk_in => PS2K_CLK_IN,
		ps2k_dat_in => PS2K_DAT_IN,
		ps2k_clk_out => PS2K_CLK_OUT,
		ps2k_dat_out => PS2K_DAT_OUT,
		
		-- Joystick
		joya => "11" & joy1_p7_io & joy1_p6_io & joy1_right_io & joy1_left_io & joy1_down_io & joy1_up_io,
		joyb => "11" & joy2_p7_io & joy2_p6_io & joy2_right_io & joy2_left_io & joy2_down_io & joy2_up_io,

		
		-- joy_s format MXYZ SACB RLDU
		--joya => joy1_s(7 downto 0), 
		--joyb => joy2_s(7 downto 0), 	
		
		-- SD card
		spi_cs => sd_cs_n_o,
		spi_miso => sd_miso_i,
		spi_mosi => sd_mosi_o,
		spi_clk => sd_sclk_o

	);
	
	
		joy1_p8_io <= '0';
		joy2_p8_io <= '0';

	mydither : work.video_vga_dither
		generic map(
			outbits => 5
	)
		port map(
			clk=>memclk,
			hsync=>vga_hsync_n_s,
			vsync=>vga_vsync_n_s,
			vid_ena=>'1',
			iRed => vga_red,
			iGreen => vga_green,
			iBlue => vga_blue,
			std_logic_vector(oRed) => vga_r_o,
			std_logic_vector(oGreen) => vga_g_o,
			std_logic_vector(oBlue) => vga_b_o
		);


	leftsd: component hybrid_pwm_sd
	port map
	(
		clk => memclk,
		n_reset => '1',
		din(15) => not audio_l(15),
		din(14 downto 0) => std_logic_vector(audio_l(14 downto 0)),
		dout => dac_l_o
	);
	
	rightsd: component hybrid_pwm_sd
	port map
	(
		clk => memclk,
		n_reset => '1',
		din(15) => not audio_r(15),
		din(14 downto 0) => std_logic_vector(audio_r(14 downto 0)),
		dout => dac_r_o
	);



	
			

end architecture;

