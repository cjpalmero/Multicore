--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.numeric_std.ALL;

entity debounce is
	generic(
		default : std_logic :='1';
		bits : integer := 12
	);
	port(
		clk : in std_logic;
		signal_in : in std_logic;
		signal_out : out std_logic;
		posedge : out std_logic;
		negedge : out std_logic
	);
end debounce;

architecture RTL of debounce is
signal counter : unsigned(bits-1 downto 0):=to_unsigned(0,bits);
signal regin : std_logic := default;	-- Deglitched input signal
signal regin2 : std_logic := default;	-- Deglitched input signal
signal debtemp : std_logic := default;
signal prev : std_logic := default;
begin

	process(clk)
	begin
		if rising_edge(clk) then
			posedge<='0';
			negedge<='0';
			regin2<=signal_in;
			regin <= regin2;
			
			if debtemp/=regin then
				counter<=(others=>'1');
				debtemp<=regin;
			else
				if counter(counter'high downto 1)=(counter'high downto 1 => '0') then
					if counter(0)='1' then
						if prev/=regin then
							posedge<=regin;
							negedge<=not regin;
							prev<=regin;
						end if;
						counter<=counter-1;
					end if;
				else
					signal_out<=regin;
					counter<=counter-1;	
				end if;
			end if;
		end if;
	end process;

end architecture;