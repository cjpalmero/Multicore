--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
--//============================================================================
--//
--//  Multicore 2+ Top by Victor Trucco
--//
--//============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity top is
    port (
  -- Clocks
        clock_50_i         : in    std_logic;

        -- Buttons
        btn_n_i            : in    std_logic_vector(4 downto 1);

        -- SRAM
        sram_addr_o        : out   std_logic_vector(20 downto 0)   := (others => '0');
        sram_data_io       : inout std_logic_vector(7 downto 0)    := (others => 'Z');
        sram_we_n_o        : out   std_logic                               := '1';
        sram_oe_n_o        : out   std_logic                               := '1';

        -- SDRAM
        SDRAM_A            : out std_logic_vector(12 downto 0);
        SDRAM_DQ           : inout std_logic_vector(15 downto 0);

        SDRAM_BA           : out std_logic_vector(1 downto 0);
        SDRAM_DQMH         : out std_logic;
        SDRAM_DQML         : out std_logic;    

        SDRAM_nRAS         : out std_logic;
        SDRAM_nCAS         : out std_logic;
        SDRAM_CKE          : out std_logic;
        SDRAM_CLK          : out std_logic;
        SDRAM_nCS          : out std_logic;
        SDRAM_nWE          : out std_logic;
    
        -- PS2
        ps2_clk_io         : inout std_logic                        := 'Z';
        ps2_data_io        : inout std_logic                        := 'Z';
        ps2_mouse_clk_io   : inout std_logic                        := 'Z';
        ps2_mouse_data_io  : inout std_logic                        := 'Z';

        -- SD Card
        sd_cs_n_o          : out   std_logic                        := 'Z';
        sd_sclk_o          : out   std_logic                        := 'Z';
        sd_mosi_o          : out   std_logic                        := 'Z';
        sd_miso_i          : in    std_logic;

        -- Joysticks
        joy_clock_o        : out   std_logic;
        joy_load_o         : out   std_logic;
        joy_data_i         : in    std_logic;
        joy_p7_o           : out   std_logic                        := '1';

        -- Audio
        AUDIO_L             : out   std_logic                       := '0';
        AUDIO_R             : out   std_logic                       := '0';
        ear_i               : in    std_logic;
        mic_o               : out   std_logic                       := '0';

        -- VGA
        VGA_R               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_G               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_B               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_HS              : out   std_logic                       := '1';
        VGA_VS              : out   std_logic                       := '1';

        LED                 : out   std_logic                       := '1';-- 0 is led on

        --STM32
        stm_rx_o            : out std_logic     := 'Z'; -- stm RX pin, so, is OUT on the slave
        stm_tx_i            : in  std_logic     := 'Z'; -- stm TX pin, so, is IN on the slave
        stm_rst_o           : out std_logic     := 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
        
        SPI_SCK             : in  std_logic;
        SPI_DO              : out std_logic   := 'Z';
        SPI_DI              : in  std_logic;
        SPI_SS2             : in  std_logic;
        SPI_nWAIT           : out std_logic   := '1';

        GPIO                : inout std_logic_vector(31 downto 0)   := (others => 'Z')
    );
end entity;

architecture Behavior of top is

    component joystick_serial is
    port
    (
        clk_i           : in  std_logic;
        joy_data_i      : in  std_logic;
        joy_clk_o       : out  std_logic;
        joy_load_o      : out  std_logic;

        joy1_up_o       : out std_logic;
        joy1_down_o     : out std_logic;
        joy1_left_o     : out std_logic;
        joy1_right_o    : out std_logic;
        joy1_fire1_o    : out std_logic;
        joy1_fire2_o    : out std_logic;
        joy2_up_o       : out std_logic;
        joy2_down_o     : out std_logic;
        joy2_left_o     : out std_logic;
        joy2_right_o    : out std_logic;
        joy2_fire1_o    : out std_logic;
        joy2_fire2_o    : out std_logic
    );
    end component;

    -- Sigma Delta audio
    COMPONENT hybrid_pwm_sd
        PORT
        (
            clk     :    IN STD_LOGIC;
            n_reset     :    IN STD_LOGIC;
            din     :    IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            dout        :    OUT STD_LOGIC
        );
    END COMPONENT;


    ----------------------------------------
    -- internal signals                   --
    ----------------------------------------

    -- clock
    signal pll_in_clk : std_logic;
    signal clk_114  : std_logic;
    signal clk_28       : std_logic;
    signal pll_locked : std_logic;
    signal clk_7        : std_logic;
    signal c1           : std_logic;
    signal c3           : std_logic;
    signal cck          : std_logic;
    signal eclk         : std_logic_vector (10-1 downto 0);
    signal clk_50       : std_logic;
    signal sysclk       : std_logic;
    signal memclk       : std_logic;
    signal clk_vga      : std_logic;
 
    -- reset
    signal pll_rst  : std_logic;
    signal sdctl_rst    : std_logic;
    signal rst_50       : std_logic;
    signal rst_minimig: std_logic;
 
    -- ctrl 
    signal rom_status : std_logic;
    signal ram_status : std_logic;
    signal reg_status : std_logic;
 
    -- tg68
    signal tg68_rst     : std_logic;
    signal tg68_dat_in  : std_logic_vector (16-1 downto 0);
    signal tg68_dat_out     : std_logic_vector (16-1 downto 0);
    signal tg68_adr         : std_logic_vector (32-1 downto 0);
    signal tg68_IPL         : std_logic_vector (3-1 downto 0);
    signal tg68_dtack   : std_logic;
    signal tg68_as      : std_logic;
    signal tg68_uds         : std_logic;
    signal tg68_lds         : std_logic;
    signal tg68_rw      : std_logic;
    signal tg68_ena7RD  : std_logic;
    signal tg68_ena7WR  : std_logic;
    signal tg68_enaWR   : std_logic;
    signal tg68_cout        : std_logic_vector (16-1 downto 0);
    signal tg68_cpuena  : std_logic;
    signal cpu_config   : std_logic_vector (2-1 downto 0);
    signal memcfg           : std_logic_vector (6-1 downto 0);
    signal tg68_cad         : std_logic_vector (32-1 downto 0);
    signal tg68_cpustate : std_logic_vector (6-1 downto 0);
    signal tg68_cdma        : std_logic;
    signal tg68_clds        : std_logic;
    signal tg68_cuds        : std_logic;
 
    -- minimig
    signal ram_data         : std_logic_vector (16-1 downto 0);      -- sram data bus
    signal ramdata_in   : std_logic_vector (16-1 downto 0);    -- sram data bus in
    signal ram_address  : std_logic_vector (22-1 downto 0);   -- sram address bus
    signal ram_bhe      : std_logic;                                -- sram upper byte select
    signal ram_ble      : std_logic;                                -- sram lower byte select
    signal ram_we           : std_logic;                                 -- sram write enable
    signal ram_oe           : std_logic;                             -- sram output enable
    signal s15khz           : std_logic;                              -- scandoubler disable
    signal joy_emu_en   : std_logic;                             -- joystick emulation enable
    signal sdo              : std_logic;                              -- SPI data output
    signal ldata            : std_logic_vector (15-1 downto 0);   -- left DAC data
    signal rdata            : std_logic_vector (15-1 downto 0);         -- right DAC data
    signal audio_left   : std_logic_vector (15 downto 0);
    signal audio_right  : std_logic_vector (15 downto 0);
    signal floppy_fwr   : std_logic;
    signal floppy_frd   : std_logic;
    signal hd_fwr           : std_logic;
    signal hd_frd           : std_logic;
 
    -- sdram
    signal reset_out    : std_logic;
    signal sdram_cs     : std_logic_vector (4-1 downto 0);
    signal sdram_dqm    : std_logic_vector (2-1 downto 0);

 
    -- audio
    signal audio_lr_switch  : std_logic;
    signal audio_lr_mix     : std_logic;
    
    signal aud_l          : signed(15 downto 0);
    signal aud_r          : signed(15 downto 0);
 
    -- ctrl
    signal SRAM_DAT_W       : std_logic_vector (16-1 downto 0);
    signal SRAM_DAT_R       : std_logic_vector (16-1 downto 0);
    signal FL_DAT_W     : std_logic_vector (8-1 downto 0);
    signal FL_DAT_R     : std_logic_vector (8-1 downto 0);
    signal SPI_CS_N     : std_logic_vector (4-1 downto 0);
    signal rst_ext          : std_logic;
    signal boot_sel     : std_logic;
    signal ctrl_cfg     : std_logic_vector (4-1 downto 0);
    signal ctrl_status  : std_logic_vector (4-1 downto 0);

    -- indicators
    signal track    : std_logic_vector (8-1 downto 0);
 
    --audio
    signal audio_LR : std_logic;
 
    signal vga_red      : unsigned (7 downto 0);
    signal vga_green    : unsigned (7 downto 0);
    signal vga_blue : unsigned (7 downto 0);    
    signal vga_hsync_n_s : std_logic;
    signal vga_vsync_n_s : std_logic;
    signal vga_blank_s : std_logic;
        
    signal lpf1_wave_L  : std_logic_vector (15 downto 0);
    signal lpf5_wave_L  : std_logic_vector (15 downto 0);
    signal lpf1_wave_R  : std_logic_vector (15 downto 0);
    signal lpf5_wave_R  : std_logic_vector (15 downto 0);

    -- PS/2 keyboard
    signal PS2K_DAT_IN      : std_logic;
    signal PS2K_CLK_IN      : std_logic;
    signal PS2K_DAT_OUT     : std_logic;
    signal PS2K_CLK_OUT     : std_logic;
 
    -- PS/2 Mouse
    signal PS2M_DAT_IN      : std_logic;
    signal PS2M_CLK_IN      : std_logic;
    signal PS2M_DAT_OUT     : std_logic;
    signal PS2M_CLK_OUT     : std_logic;
    
    -- joystick
    signal joy1_s           : std_logic_vector(11 downto 0) := (others => '1'); -- MS ZYX CBA RLDU  
    signal joy2_s           : std_logic_vector(11 downto 0) := (others => '1'); -- MS ZYX CBA RLDU  
    signal joyP7_s          : std_logic;
    

    signal div_clk : std_logic_vector (4 downto 0) := (others=>'0');
    
  signal joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i : std_logic;
  signal joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i : std_logic;

BEGIN

stm_rst_o <= '0';

 joystick_serial1 : joystick_serial 
    port map
    (
        clk_i           => div_clk(1),
        joy_data_i      => joy_data_i,
        joy_clk_o       => joy_clock_o,
        joy_load_o      => joy_load_o,

        joy1_up_o       => joy1_up_i,
        joy1_down_o     => joy1_down_i,
        joy1_left_o     => joy1_left_i,
        joy1_right_o    => joy1_right_i,
        joy1_fire1_o    => joy1_p6_i,
        joy1_fire2_o    => joy1_p9_i,

        joy2_up_o       => joy2_up_i,
        joy2_down_o     => joy2_down_i,
        joy2_left_o     => joy2_left_i,
        joy2_right_o    => joy2_right_i,
        joy2_fire1_o    => joy2_p6_i,
        joy2_fire2_o    => joy2_p9_i
    );


PS2K_DAT_IN <= ps2_data_io;
PS2K_CLK_IN <= ps2_clk_io;
PS2M_DAT_IN <= ps2_mouse_data_io;
PS2M_CLK_IN <= ps2_mouse_clk_io;

ps2_data_io         <= '0' when (PS2K_DAT_OUT = '0') else 'Z'; 
ps2_clk_io          <= '0' when (PS2K_CLK_OUT = '0') else 'Z'; 
ps2_mouse_data_io   <= '0' when (PS2M_DAT_OUT = '0') else 'Z'; 
ps2_mouse_clk_io    <= '0' when (PS2M_CLK_OUT = '0') else 'Z'; 

--vga_r_o <= vga_red(7 downto 4);
--vga_g_o <= vga_green(7 downto 4);
--vga_b_o <= vga_blue(7 downto 4);
VGA_VS <= vga_vsync_n_s;
VGA_HS <= vga_hsync_n_s;



    U00: work.pll 
    port map 
    (
        inclk0      => clock_50_i,      -- external Clock 50 MHz 
        c0              => sysclk,          -- 41.667
        c1              => memclk,          -- 125 mhz
        c2              => SDRAM_CLK, -- 125 mhz shifted
        c3              => clk_vga          -- 25 mhz
    );
    

process(sysclk)
    begin
        if rising_edge(sysclk) then
            div_clk <= div_clk + 1;
        end if;
end process;

--  u_interpo_L: work.INTERPO 
--  generic map
--  (
--      msbi => 15
--  )
--  port map 
--  (
--      clk21m  => sysclk,
--      reset   => '0',
--      clkena  => '1',
--      idata   => "00" & audio_left(15 downto 2),
--      odata   => lpf1_wave_L
--  );
--  
--  u_lpf2_L: work.lpf2 
--  generic map
--  (
--      msbi => 15
--  )
--  port map 
--  (
--      clk21m  => sysclk,
--      reset   => '0',
--      clkena  => '1',
--      idata   => lpf1_wave_L,
--      odata   => lpf5_wave_L
--  );
--
--  u_interpo_R: work.INTERPO 
--  generic map
--  (
--      msbi => 15
--  )
--  port map 
--  (
--      clk21m  => sysclk,
--      reset   => '0',
--      clkena  => '1',
--      idata   => "00" & audio_right(15 downto 2),
--      odata   => lpf1_wave_R
--  );
--  
--  u_lpf2_R: work.lpf2 
--  generic map
--  (
--      msbi => 15
--  )
--  port map 
--  (
--      clk21m  => sysclk,
--      reset   => '0',
--      clkena  => '1',
--      idata   => lpf1_wave_R,
--      odata   => lpf5_wave_R
--  );
--
--
--  dacL: work.dac 
--  generic map
--  (
--      msbi_g => 15
--  )
--  port map 
--  (
--      clk_i   => sysclk,
--      res_n_i  => '1',
--      dac_i       => lpf5_wave_L,
--      dac_o       => dac_l_o
--  );
--   
--  dacR: work.dac 
--  generic map
--  (
--      msbi_g => 15
--  )
--  port map 
--  (
--      clk_i   => sysclk,
--      res_n_i  => '1',
--      dac_i       => lpf5_wave_R,
--      dac_o       => dac_r_o
--  );


    myvt: work.Virtual_Toplevel 
    generic map
    (
        rowAddrBits => 13,
        colAddrBits => 9
    )
    port map 
    (
        reset => btn_n_i(1),
        CLK => sysclk,
        SDR_CLK => memclk,
        
        --SW => SW,
        
        DRAM_ADDR => SDRAM_A,
        DRAM_DQ => SDRAM_DQ,
        DRAM_BA_1 => SDRAM_BA(1),
        DRAM_BA_0 => SDRAM_BA(0),
        DRAM_CKE => SDRAM_CKE,
        DRAM_UDQM => SDRAM_DQMH,
        DRAM_LDQM => SDRAM_DQML,
        DRAM_CS_N => SDRAM_nCS,
        DRAM_WE_N => SDRAM_nWE,
        DRAM_CAS_N => SDRAM_nCAS,
        DRAM_RAS_N => SDRAM_nRAS,
        
--      DAC_LDATA => audio_left,
--      DAC_RDATA => audio_right,
        signed(DAC_LDATA) => aud_l,
        signed(DAC_RDATA) => aud_r,
        
        unsigned(VGA_R) => vga_red,
        unsigned(VGA_G) => vga_green,
        unsigned(VGA_B) => vga_blue,
        VGA_VS => vga_vsync_n_s,
        VGA_HS => vga_hsync_n_s,
        VGA_BLANK => vga_blank_s,
        
        RS232_RXD => stm_tx_i,
        RS232_TXD => stm_rx_o,
        
        -- PS/2
        ps2k_clk_in => PS2K_CLK_IN,
        ps2k_dat_in => PS2K_DAT_IN,
        ps2k_clk_out => PS2K_CLK_OUT,
        ps2k_dat_out => PS2K_DAT_OUT,
        
        -- Joystick
        --joya => "11" & joy1_p9_i & joy1_p6_i & joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i,
        --joyb => "11" & joy2_p9_i & joy2_p6_i & joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i,

        -- joy_s format MXYZ SACB RLDU
        joya => joy1_s(7 downto 0), 
        joyb => joy2_s(7 downto 0),     
        
        -- SD card
        spi_cs => sd_cs_n_o,
        spi_miso => sd_miso_i,
        spi_mosi => sd_mosi_o,
        spi_clk => sd_sclk_o

    );
    

    mydither : work.video_vga_dither
        generic map(
            outbits => 5
    )
        port map(
            clk=>memclk,
            hsync=>vga_hsync_n_s,
            vsync=>vga_vsync_n_s,
            vid_ena=>'1',
            iRed => vga_red,
            iGreen => vga_green,
            iBlue => vga_blue,
            std_logic_vector(oRed) => VGA_R,
            std_logic_vector(oGreen) => VGA_G,
            std_logic_vector(oBlue) => VGA_B
        );


    leftsd: component hybrid_pwm_sd
    port map
    (
        clk => memclk,
        n_reset => '1',
        din(15) => not aud_l(15),
        din(14 downto 0) => std_logic_vector(aud_l(14 downto 0)),
        dout => AUDIO_L
    );
    
    rightsd: component hybrid_pwm_sd
    port map
    (
        clk => memclk,
        n_reset => '1',
        din(15) => not aud_r(15),
        din(14 downto 0) => std_logic_vector(aud_r(14 downto 0)),
        dout => AUDIO_R
    );

--- Joystick read with sega 6 button support----------------------

    process(vga_hsync_n_s)
        variable state_v : unsigned(7 downto 0) := (others=>'0');
        variable j1_sixbutton_v : std_logic := '0';
        variable j2_sixbutton_v : std_logic := '0';
    begin
        if falling_edge(vga_hsync_n_s) then
        
            state_v := state_v + 1;
            
            case state_v is
                -- joy_s format MXYZ SACB RLDU
            
                when X"00" =>  
                    joyP7_s <= '0';
                    
                when X"01" =>
                    joyP7_s <= '1';

                when X"02" => 
                    joy1_s(3 downto 0) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- R, L, D, U
                    joy2_s(3 downto 0) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- R, L, D, U
                    joy1_s(5 downto 4) <= joy1_p9_i & joy1_p6_i; -- C, B
                    joy2_s(5 downto 4) <= joy2_p9_i & joy2_p6_i; -- C, B                    
                    joyP7_s <= '0';
                    j1_sixbutton_v := '0'; -- Assume it's not a six-button controller
                    j2_sixbutton_v := '0'; -- Assume it's not a six-button controller

                when X"03" =>
                    joy1_s(7 downto 6) <= joy1_p9_i & joy1_p6_i; -- Start, A
                    joy2_s(7 downto 6) <= joy2_p9_i & joy2_p6_i; -- Start, A
                    joyP7_s <= '1';
            
                when X"04" =>  
                    joyP7_s <= '0';

                when X"05" =>
                    if joy1_right_i = '0' and joy1_left_i = '0' and joy1_down_i = '0' and joy1_up_i = '0' then 
                        j1_sixbutton_v := '1'; --it's a six button
                    end if;
                    
                    if joy2_right_i = '0' and joy2_left_i = '0' and joy2_down_i = '0' and joy2_up_i = '0' then 
                        j2_sixbutton_v := '1'; --it's a six button
                    end if;
                    
                    joyP7_s <= '1';
                    
                when X"06" =>
                    if j1_sixbutton_v = '1' then
                        joy1_s(11 downto 8) <= joy1_right_i & joy1_left_i & joy1_down_i & joy1_up_i; -- Mode, X, Y e Z
                    end if;
                    
                    if j2_sixbutton_v = '1' then
                        joy2_s(11 downto 8) <= joy2_right_i & joy2_left_i & joy2_down_i & joy2_up_i; -- Mode, X, Y e Z
                    end if;
                    
                    joyP7_s <= '0';

                when others =>
                    joyP7_s <= '1';
                    
            end case;

        end if;
    end process;
    
    joy_p7_o <= joyP7_s;
---------------------------

    
end architecture;

