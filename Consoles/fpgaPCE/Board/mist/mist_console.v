/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// mist_console.v
//
// receive serial data and forware it to the io controller
// 

module mist_console #(parameter CLKFREQ=100) (
		     // system interface
		     input 	  clk,      // 125MHz
		     input 	  n_reset,
	
		     input 	  ser_in,

		     //	input par_out_
		     output [7:0] par_out_data,
		     output   par_out_strobe
); 

localparam TICKSPERBIT = (CLKFREQ*1000000)/115200;

assign par_out_data = rx_byte;
assign par_out_strobe = strobe;
 
reg strobe;
reg [7:0] rx_byte /* synthese noprune */;
reg [5:0] state;
reg [15:0] recheck; 

always @(posedge clk) begin
	if(!n_reset) begin
		state <= 6'd0;  // idle
		strobe <= 1'b0;
	end else begin
		if(state == 0) begin
 
			// detecting low in idle state
			if(!ser_in) begin 
				recheck <= 3*TICKSPERBIT/2;
				state <= 9;
				strobe <= 1'b0;
			end
		end else begin
			if(recheck != 0) 
				recheck <= recheck - 1;
			else begin
				if(state > 1)
					rx_byte <= { ser_in, rx_byte[7:1]};
					
				recheck <= TICKSPERBIT;
				state <= state - 1;
				
				// last bit is stop bit and needs to be '1'
				if((state == 1) && (ser_in == 1))
					strobe <= 1'b1;
			end
		end
	end
end

endmodule
