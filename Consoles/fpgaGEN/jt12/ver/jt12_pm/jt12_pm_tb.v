/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/
`timescale 1ns / 1ps

module jt12_pm_tb;

reg [4:0] lfo_mod=5'd0;
reg [10:0] fnum = 11'd1;
reg clk;
reg [2:0] pms=3'd0;

initial begin
	clk = 1'b0;
	forever clk = #10 ~clk;
end // initial

reg finish=1'b0;

always @(posedge clk) begin
	lfo_mod <= lfo_mod + 1'd1;
	if( &lfo_mod ) pms<=pms+3'd1;
	if( &{lfo_mod, pms} ) { finish, fnum} <= { fnum, 1'b0 };
	if( finish ) $finish;
	$display("%d\t%d\t0x%X\t%d",lfo_mod,pms,fnum,pm_offset);
end

// initial begin
// 	$dumpfile("jt12_pm_tb.lxt");
// 	$dumpvars;
// 	$dumpon;
// end

wire signed [7:0] pm_offset;

jt12_pm uut (
	.lfo_mod( lfo_mod ),
	.pms(pms),
	.fnum( fnum ),
	.pm_offset( pm_offset )
);

endmodule // jt12_pm