--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;

package sdram is
component sdram
	port (

-- System
		clk    : in std_logic;
		init_n : in std_logic;

-- SDRAM interface
		SDRAM_DQ : inout std_logic_vector(15 downto 0);
		SDRAM_A : out std_logic_vector(12 downto 0);
		SDRAM_nWE : out std_logic;
		SDRAM_nRAS : out std_logic;
		SDRAM_nCAS : out std_logic;
		SDRAM_BA : out std_logic_vector(1 downto 0);
		SDRAM_DQML : out std_logic;
		SDRAM_DQMH : out std_logic;
		SDRAM_nCS : out std_logic;

-- SDRAM ports
		romwr_req : in std_logic;
		romwr_ack : out std_logic;
		romwr_a : in std_logic_vector(23 downto 1);
		romwr_d : in std_logic_vector(15 downto 0);

		romrd_req : in std_logic;
		romrd_ack : out std_logic;
		romrd_a : in std_logic_vector(23 downto 1);
		romrd_q : out std_logic_vector(15 downto 0);

		ram68k_req : in std_logic;
		ram68k_ack : out std_logic;
		ram68k_we : in std_logic;
		ram68k_a : in std_logic_vector(15 downto 1);
		ram68k_d : in std_logic_vector(15 downto 0);
		ram68k_q : out std_logic_vector(15 downto 0);
		ram68k_u_n : in std_logic;
		ram68k_l_n : in std_logic;

		sram_req : in std_logic;
		sram_ack : out std_logic;
		sram_we : in std_logic;
		sram_a : in std_logic_vector(15 downto 1);
		sram_d : in std_logic_vector(15 downto 0);
		sram_q : out std_logic_vector(15 downto 0);
		sram_u_n : in std_logic;
		sram_l_n : in std_logic;

		vram_req : in std_logic;
		vram_ack : out std_logic;
		vram_we : in std_logic;
		vram_a : in std_logic_vector(15 downto 1);
		vram_d : in std_logic_vector(15 downto 0);
		vram_q : out std_logic_vector(15 downto 0);
		vram_u_n : in std_logic;
		vram_l_n : in std_logic;

		vram32_req : in std_logic;
		vram32_ack : out std_logic;
		vram32_a   : in std_logic_vector(15 downto 1);
		vram32_q   : out std_logic_vector(31 downto 0);

		svp_ram1_req : in std_logic;
		svp_ram1_ack : out std_logic;
		svp_ram1_we : in std_logic;
		svp_ram1_a : in std_logic_vector(16 downto 1);
		svp_ram1_d : in std_logic_vector(15 downto 0);
		svp_ram1_q : out std_logic_vector(15 downto 0);

		svp_ram2_req : in std_logic;
		svp_ram2_ack : out std_logic;
		svp_ram2_we : in std_logic;
		svp_ram2_a : in std_logic_vector(16 downto 1);
		svp_ram2_d : in std_logic_vector(15 downto 0);
		svp_ram2_q : out std_logic_vector(15 downto 0);
		svp_ram2_u_n : in std_logic;
		svp_ram2_l_n : in std_logic;

		svp_rom_req : in std_logic;
		svp_rom_ack : out std_logic;
		svp_rom_a : in std_logic_vector(23 downto 1);
		svp_rom_q : out std_logic_vector(15 downto 0)
);
end component;
end package;
