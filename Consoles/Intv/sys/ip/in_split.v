/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// in_split.v


`timescale 1 ps / 1 ps
module in_split (
		input  wire        clk,                //  input.clk
		input  wire        ce,                 //       .ce
		input  wire        de,                 //       .de
		input  wire        h_sync,             //       .h_sync
		input  wire        v_sync,             //       .v_sync
		input  wire        f,                  //       .f
		input  wire [23:0] data,               //       .data
		output wire        vid_clk,            // Output.vid_clk
		output reg         vid_datavalid,      //       .vid_datavalid
		output reg  [1:0]  vid_de,             //       .vid_de
		output reg  [1:0]  vid_f,              //       .vid_f
		output reg  [1:0]  vid_h_sync,         //       .vid_h_sync
		output reg  [1:0]  vid_v_sync,         //       .vid_v_sync
		output reg  [47:0] vid_data,           //       .vid_data
		output wire        vid_locked,         //       .vid_locked
		output wire [7:0]  vid_color_encoding, //       .vid_color_encoding
		output wire [7:0]  vid_bit_width,      //       .vid_bit_width
		input  wire        clipping,           //       .clipping
		input  wire        overflow,           //       .overflow
		input  wire        sof,                //       .sof
		input  wire        sof_locked,         //       .sof_locked
		input  wire        refclk_div,         //       .refclk_div
		input  wire        padding             //       .padding
	);

	assign vid_bit_width = 0;
	assign vid_color_encoding = 0;
	assign vid_locked = 1;
	assign vid_clk = clk;

	always @(posedge clk) begin
		reg odd = 0;
		
		vid_datavalid <= 0;
		if(ce) begin
			vid_de[odd] <= de;
			vid_f[odd] <= f;
			vid_h_sync[odd] <= h_sync;
			vid_v_sync[odd] <= v_sync;
			if(odd) vid_data[47:24] <= data;
				else vid_data[23:0] <= data;
			
			odd <= ~odd;
			vid_datavalid <= odd;
		end
	end
endmodule
