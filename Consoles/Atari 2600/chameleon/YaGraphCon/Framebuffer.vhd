--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- Copyright (c) 2009 Frank Buss (fb@frank-buss.de)
-- See license.txt for license

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.YaGraphConPackage.all;

entity Framebuffer is
	generic(
		ADDRESS_WIDTH: natural;
		BIT_DEPTH: natural
	);
	port(
		clock: in std_logic;

		-- 1st RAM port for read-only access
		readAddress1: in unsigned(ADDRESS_WIDTH-1 downto 0);
		q1: out unsigned(BIT_DEPTH-1 downto 0);

		-- 2nd RAM port for read-only access
		readAddress2: in unsigned(ADDRESS_WIDTH-1 downto 0);
		q2: out unsigned(BIT_DEPTH-1 downto 0);

		-- 3rd RAM port for write access
		writeAddress: in unsigned(ADDRESS_WIDTH-1 downto 0);
		data: in unsigned(BIT_DEPTH-1 downto 0);
		writeEnable: in std_logic
	);
end entity Framebuffer;

architecture rtl of Framebuffer is

	-- infering template for Xilinx block RAM
	constant ADDR_WIDTH : integer := ADDRESS_WIDTH;
	constant DATA_WIDTH : integer := BIT_DEPTH;
	type framebufferType is array (2**ADDR_WIDTH-1 downto 0) of unsigned(DATA_WIDTH-1 downto 0);
	signal framebufferRam1: framebufferType;
	signal framebufferRam2: framebufferType;

begin

	-- infering template for Xilinx block RAM
	ram1: process(clock)
	begin
		if rising_edge(clock) then
		--if (clock'event and clock = '1') then
			if writeEnable = '1' then
				framebufferRam1(to_integer(writeAddress)) <= data;
			end if;
			q1 <= framebufferRam1(to_integer(readAddress1));
		end if;
	end process;

	-- infering template for Xilinx block RAM
	ram2: process(clock)
	begin
		if rising_edge(clock) then
		--if (clock'event and clock = '1') then
			if writeEnable = '1' then
				framebufferRam2(to_integer(writeAddress)) <= data;
			end if;
			q2 <= framebufferRam2(to_integer(readAddress2));
		end if;
	end process;

end architecture rtl;
