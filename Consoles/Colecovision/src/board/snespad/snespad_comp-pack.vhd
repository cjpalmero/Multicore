--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-------------------------------------------------------------------------------
--
-- SNESpad controller core
--
-- Copyright (c) 2004, Arnim Laeuger (arniml@opencores.org)
--
-- $Id: snespad_comp-pack.vhd,v 1.1 2004/10/05 18:20:14 arniml Exp $
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package snespad_comp is

  component snespad
    generic (
      num_pads_g       :     natural := 1;
      reset_level_g    :     natural := 0;
      button_level_g   :     natural := 0;
      clocks_per_6us_g :     natural := 6
    );
    port (
      clk_i            : in  std_logic;
      reset_i          : in  std_logic;
      pad_clk_o        : out std_logic;
      pad_latch_o      : out std_logic;
      pad_data_i       : in  std_logic_vector(num_pads_g-1 downto 0);
      but_a_o          : out std_logic_vector(num_pads_g-1 downto 0);
      but_b_o          : out std_logic_vector(num_pads_g-1 downto 0);
      but_x_o          : out std_logic_vector(num_pads_g-1 downto 0);
      but_y_o          : out std_logic_vector(num_pads_g-1 downto 0);
      but_start_o      : out std_logic_vector(num_pads_g-1 downto 0);
      but_sel_o        : out std_logic_vector(num_pads_g-1 downto 0);
      but_tl_o         : out std_logic_vector(num_pads_g-1 downto 0);
      but_tr_o         : out std_logic_vector(num_pads_g-1 downto 0);
      but_up_o         : out std_logic_vector(num_pads_g-1 downto 0);
      but_down_o       : out std_logic_vector(num_pads_g-1 downto 0);
      but_left_o       : out std_logic_vector(num_pads_g-1 downto 0);
      but_right_o      : out std_logic_vector(num_pads_g-1 downto 0)
    );
  end component snespad;

end snespad_comp;
