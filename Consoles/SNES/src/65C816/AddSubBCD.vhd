--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.P65816_pkg.all;

entity AddSubBCD is
	port( 
		A		: in std_logic_vector(15 downto 0); 
		B		: in std_logic_vector(15 downto 0); 
		CI		: in std_logic;
		ADD	: in std_logic;
		BCD	: in std_logic; 
		w16	: in std_logic; 
		S		: out std_logic_vector(15 downto 0);
		CO		: out std_logic;
		VO		: out std_logic
    );
end AddSubBCD;

architecture rtl of AddSubBCD is

	signal VO1, VO3 : std_logic;
	signal CO0, CO1, CO2, CO3 : std_logic;

begin
	
	add0 : entity work.BCDAdder
	port map (
		A => A(3 downto 0),
		B => B(3 downto 0),
		CI => CI,
		
		S => S(3 downto 0),
		CO => CO0,
		
		ADD => ADD,
		BCD => BCD
	);
	
	add1 : entity work.BCDAdder
	port map (
		A => A(7 downto 4),
		B => B(7 downto 4),
		CI => CO0,
		
		S => S(7 downto 4),
		CO => CO1,
		VO => VO1,
		
		ADD => ADD,
		BCD => BCD
	);
	
	add2 : entity work.BCDAdder
	port map (
		A => A(11 downto 8),
		B => B(11 downto 8),
		CI => CO1,
		
		S => S(11 downto 8),
		CO => CO2,
		
		ADD => ADD,
		BCD => BCD
	);
	
	add3 : entity work.BCDAdder
	port map (
		A => A(15 downto 12),
		B => B(15 downto 12),
		CI => CO2,
		
		S => S(15 downto 12),
		CO => CO3,
		VO => VO3,
		
		ADD => ADD,
		BCD => BCD
	);
	
	
	VO <= VO1 when w16 = '0' else VO3;
	CO <= CO1 when w16 = '0' else CO3;

end rtl;