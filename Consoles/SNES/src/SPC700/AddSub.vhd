--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.SPC700_pkg.all;

entity SPC700_AddSub is
    port( 
        A		: in std_logic_vector(7 downto 0);  
		  B		: in std_logic_vector(7 downto 0);  
		  CI		: in std_logic; 
		  ADD		: in std_logic; 
        S		: out std_logic_vector(7 downto 0);
        CO		: out std_logic;
		  VO		: out std_logic;
		  HO		: out std_logic
    );
end SPC700_AddSub;

architecture rtl of SPC700_AddSub is

	signal tempB : std_logic_vector(7 downto 0);
	signal res : unsigned(7 downto 0);
	signal C7 : std_logic;
	
begin

	tempB <= B when ADD = '1' else B xor x"FF";

	process(A, tempB, CI, ADD)
		variable temp0, temp1 : unsigned(4 downto 0);
	begin
		temp0 := ('0' & unsigned(A(3 downto 0))) + ('0' & unsigned(tempB(3 downto 0))) + ("0000" & CI);
		temp1 := ('0' & unsigned(A(7 downto 4))) + ('0' & unsigned(tempB(7 downto 4))) + ("0000" & temp0(4));
		
		res <= temp1(3 downto 0) & temp0(3 downto 0);
		C7 <= temp1(4);
	end process;
	
	S <= std_logic_vector(res);
	VO <= (not (A(7) xor tempB(7))) and (A(7) xor res(7));
	HO <= (A(4) xor B(4) xor res(4)) xor not ADD;
	CO <= C7;

end rtl;