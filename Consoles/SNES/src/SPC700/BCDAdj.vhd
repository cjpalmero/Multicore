--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.SPC700_pkg.all;

entity SPC700_BCDAdj is
    port( 
        A		: in std_logic_vector(7 downto 0);   
		  ADD		: in std_logic; 
		  CI		: in std_logic; 
		  HI		: in std_logic; 
        R		: out std_logic_vector(7 downto 0);
        CO		: out std_logic
    );
end SPC700_BCDAdj;

architecture rtl of SPC700_BCDAdj is

	signal res : unsigned(7 downto 0);
	signal tempC : std_logic;
	
begin

	process(A, CI, HI, ADD)
		variable temp0, temp1 : unsigned(7 downto 0);
	begin
		temp0 := unsigned(A);
		tempC <= CI;
		
		temp1 := temp0;
		if CI = not ADD or temp0 > x"99" then
			if ADD = '0' then
				temp1 := temp0 + x"60";
			else
				temp1 := temp0 - x"60";
			end if;
			tempC <= not ADD;
		end if;
		
		res <= temp1;
		if HI = not ADD or temp1(3 downto 0) > x"9" then
			if ADD = '0' then
				res <= temp1 + x"06";
			else
				res <= temp1 - x"06";
			end if;
		end if;
	end process;
	
	R <= std_logic_vector(res);
	CO <= tempC;

end rtl;