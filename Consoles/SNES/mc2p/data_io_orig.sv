/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///
// data_io.v
//
// data_io for the MiST board
// http://code.google.com/p/mist-board/
//
// Copyright (c) 2014 Till Harbaum <till@harbaum.org>
// Copyright (c) 2015-2017 Sorgelig
// Copyright (c) 2019 György Szombathelyi
//
// This source file is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This source file is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////

module data_io # (
parameter STRLEN		=	0
)
(
	input             clk_sys,

	// Global SPI clock from ARM. 24MHz
	input             SPI_SCK,
	input             SPI_SS2,
	input             SPI_DI,
	output            SPI_DO,
	
	input   [7:0]           data_in,
	input [(8*STRLEN)-1:0] conf_str,
	output reg [31:0] status,
	output reg  [7:0] 	config_buffer_o[15:0],  // 15 bytes for general use
	
	// ARM -> FPGA download
	output reg        ioctl_download = 0, // signal indicating an active download
	output reg  [7:0] ioctl_index,        // menu index used to upload the file
	output reg        ioctl_wr = 0,
	output reg [24:0] ioctl_addr,
	output reg [15:0] ioctl_dout
);

///////////////////////////////   DOWNLOADING   ///////////////////////////////

localparam UIO_FILE_TX      = 8'h53;
localparam UIO_FILE_TX_DAT  = 8'h54;
localparam UIO_FILE_INDEX   = 8'h55;
// SPI receiver IO -> FPGA

reg sdo_s;

assign SPI_DO = sdo_s;

reg       spi_receiver_strobe_r = 0;
reg       spi_transfer_end_r = 1;
reg [7:0] spi_byte_in;

reg [7:0]	ACK = 8'd75; // letter K - 0x4b
reg  [7:0]  byte_cnt;   // counts bytes
reg  [7:0] acmd;
	reg  [2:0] bit_cnt;

// SPI MODE 0 : incoming data on Rising, outgoing on Falling
	always@(negedge SPI_SCK, posedge SPI_SS2) 
	begin
	
		
				//each time the SS goes down, we will receive a command from the SPI master
				if (SPI_SS2) // not selected
					begin
						sdo_s <= 1'bZ;
						byte_cnt <= 7'd0;
					end
				else
					begin
							
							if (acmd == 8'h10 ) //command 0x10 - send the data to the microcontroller
								sdo_s <= data_in[~bit_cnt[2:0]];
								
							else if (acmd == 8'h00 ) //command 0x00 - ACK
								sdo_s <= ACK[~bit_cnt[2:0]];
							
						//	else if (cmd == 8'h61 ) //command 0x61 - echo the pumped data
						//		sdo_s <= sram_data_s[~cnt[2:0]];			
					
					
							else if(acmd == 8'h14) //command 0x14 - reading config string
								begin
								
									if(byte_cnt < STRLEN + 1 ) // returning a byte from string
										sdo_s <= conf_str[{STRLEN - byte_cnt,~bit_cnt[2:0]}];
									else
										sdo_s <= 1'b0;
										
								end	
						
				
							if(bit_cnt[2:0] == 7) 
								byte_cnt <= byte_cnt + 8'd1;
							
					end
	end
	
// data_io has its own SPI interface to the io controller
// Read at spi_sck clock domain, assemble bytes for transferring to clk_sys
always@(posedge SPI_SCK or posedge SPI_SS2) begin

	reg  [6:0] sbuf;


	if(SPI_SS2) begin
		spi_transfer_end_r <= 1;
		bit_cnt <= 0;
	end else begin
		spi_transfer_end_r <= 0;
		
		bit_cnt <= bit_cnt + 1'd1;

		if(bit_cnt != 7)
			sbuf[6:0] <= { sbuf[5:0], SPI_DI };

		// finished reading a byte, prepare to transfer to clk_sys
        if(bit_cnt == 7) begin
			spi_byte_in <= { sbuf, SPI_DI};
			spi_receiver_strobe_r <= ~spi_receiver_strobe_r;
		end
	end
end

always @(posedge clk_sys) begin

	reg        spi_receiver_strobe;
	reg        spi_transfer_end;
	reg        spi_receiver_strobeD;
	reg        spi_transfer_endD;

	reg  [2:0] abyte_cnt;   // counts bytes
	reg [24:0] addr;
	reg        hi;
	
	reg  [4:0] cnf_byte;

	//synchronize between SPI and sys clock domains
	spi_receiver_strobeD <= spi_receiver_strobe_r;
	spi_receiver_strobe <= spi_receiver_strobeD;
	spi_transfer_endD <= spi_transfer_end_r;
	spi_transfer_end <= spi_transfer_endD;

	if (~spi_transfer_endD & spi_transfer_end) begin
		abyte_cnt <= 3'd0;
	end else if (spi_receiver_strobeD ^ spi_receiver_strobe) begin
		if(~&abyte_cnt) abyte_cnt <= abyte_cnt + 1'd1; //conta de 0 a 7 e para no 7

		if(abyte_cnt == 0) begin //executa somente no primeiro ciclo
			acmd <= spi_byte_in;
			hi <= 0;
			
			cnf_byte <= 4'd15;
			
			if (spi_byte_in == 8'h61) begin
						addr <= 0;
						ioctl_download <= 1; 
					end 
					
			if (spi_byte_in == 8'h62) begin
						ioctl_addr <= addr;
						ioctl_download <= 0;
					end
			
			
		end else begin
			case (acmd)
			
			// command 0x15: stores the status word (menu selections)
					8'h15: begin
							case (cnf_byte)	
												
								4'd15: status[31:24] <= spi_byte_in;
								4'd14: status[23:16] <= spi_byte_in;
								4'd13: status[15:8]  <= spi_byte_in;
								4'd12: status[7:0]   <= spi_byte_in;
								
							endcase
							
							cnf_byte <= cnf_byte - 1'd1;

						end
				
				8'h61: begin
					ioctl_addr <= addr;
					if (hi) ioctl_dout[15:8] <= spi_byte_in; else ioctl_dout[7:0] <= spi_byte_in;
					hi <= ~hi;
					if (hi) begin
						ioctl_wr <= ~ioctl_wr;
						ioctl_addr <= addr;
						addr <= addr + 2'd2;
					end
				end

				/*
				UIO_FILE_TX: begin
				// prepare 
					if(spi_byte_in) begin
						addr <= 0;
						ioctl_download <= 1; 
					end else begin
						ioctl_addr <= addr;
						ioctl_download <= 0;
					end
				end

				// transfer
				UIO_FILE_TX_DAT: begin
					ioctl_addr <= addr;
					if (hi) ioctl_dout[15:8] <= spi_byte_in; else ioctl_dout[7:0] <= spi_byte_in;
					hi <= ~hi;
					if (hi) begin
						ioctl_wr <= ~ioctl_wr;
						ioctl_addr <= addr;
						addr <= addr + 2'd2;
					end
				end

				// expose file (menu) index
				UIO_FILE_INDEX: ioctl_index <= spi_byte_in;
				
				*/
				
			endcase
		end
	end

end

endmodule
